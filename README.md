SpinorAlgebra 3d
====

Mathematica tool to perform computations in three dimensional N=2 superspace using the index-free formalism.

Getting started
---

###  Requisites

This package has been developed and tested on *Mathematica 11*.

### Installing

The package can be loaded in a Mathematica session by runninng
```
<<SpinorAlgebra3d`
```

after putting the file *SpinorAlgebra3d.wl* into a folder contained in `$Path`. Otherwise it is also possible to put SpinorAlgebra3d.wl in any folder and load it with
```
Get["<path-to-folder>/SpinorAlgebra3d.wl"];
```

Documentation
---

A documentation notebook is provided in *Documentation.nb*.
Every public function of this package has an help message which can be accessed with
```
?<name_of_function>
```

---

Feel free to contact me at
andrea **[dot]** manenti **[at]** physics **[dot]** uu **[dot]** se
