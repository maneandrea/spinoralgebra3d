(* ::Package:: *)

(* ::Title:: *)
(*Spinor Algebra 3d*)


(* ::Section::Closed:: *)
(*Beginning of package and function descriptions*)


(*To give them the Global` context*)
{\[Theta]1,\[Theta]2,\[Theta]3,\[Theta]b1,\[Theta]b2,\[Theta]b3,\[Theta]1sq,\[Theta]b1sq,\[Theta]2sq,\[Theta]b2sq,\[Theta]3sq,\[Theta]b3sq,
 \[Eta]1,\[Eta]2,\[Eta]3, \[Eta]999, \[Eta]899,
 x12,x13,x23,x12sq,x13sq,x23sq};


(*BeginPackage["SpinorAlgebra3d`"];*)

FN::usage="FN[\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] returns the fermion number (0 if boson, 1 if fermion) of \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). Only terms that have an odd number of \[Theta] or \[Theta]b are fermionic.";
u::usage="u[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Operator to apply on functions to make them act on and return in compact notation: \!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)[\[LeftAngleBracket]user notation\[RightAngleBracket]] -> u[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\[LeftAngleBracket]compact notation\[RightAngleBracket]].";
uu::usage="uu[\!\(\*StyleBox[\"f\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Operator to apply on functions to make them act on and return in compact notation. Similar to \!\(\*StyleBox[\"u\",\nFontFamily->\"DejaVu Sans Mono\"]\) but applies the transformation to the first two arguments.";
SplitMonomials::usage="SplitMonomials[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Transforms from compact notation to user notation: e.g. \[Eta]1x12\[Theta]b2 \[Rule] d[\[Eta]1, x12, \[Theta]b2].";
WriteMonomials::usage="WriteMonomials[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Transforms from user notation to compact notation: e.g. d[\[Eta]1, x12, \[Theta]b2] \[Rule] \[Eta]1x12\[Theta]b2";
ConstQ::usage="ConstQ[\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Returns True if \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) is considered a constant and False otherwise.";
makeConst::usage="makeConst[\!\(\*StyleBox[\"symbols\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Makes all Symbols or Expressions in \!\(\*StyleBox[\"symbols\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) return True when given to ConstQ.";
d::usage="d[\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Represents the fully contracted monomial \!\(\*StyleBox[\"ab...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), where, for example,  \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(\[Eta]\), \(2\)]\) = \!\(\*SubsuperscriptBox[\(\[Eta]\), \(1\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(\[Eta]\), \(2  \[Alpha]\)]\),  \!\(\*SubscriptBox[\(\[Eta]\), \(1\)]\)\!\(\*SubscriptBox[\(x\), \(12\)]\)\!\(\*SubscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], \(2\)]\) = \!\(\*SubsuperscriptBox[\(\[Eta]\), \(1\), \(\[Alpha]\)]\)\!\(\*SubscriptBox[\(x\), \(12  \[Alpha] \*OverscriptBox[\(\[Alpha]\), \(.\)]\)]\)\!\(\*SuperscriptBox[OverscriptBox[\(\[Eta]\), \(_\)], OverscriptBox[\(\[Alpha]\), \(.\)]]\), etc ...";
glue::usage="glue[\!\(\*StyleBox[\"a\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"...\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Returns a Symbol obtained concatenating \!\(\*StyleBox[\"ab\[CenterDot]\[CenterDot]\[CenterDot]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) .";
defineD::usage="defineD[\!\(\*StyleBox[\"symbol\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"operator\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"derivativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"multiplicativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Assigns to the Symbol \!\(\*StyleBox[\"symbol\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) a differential operator \!\(\*StyleBox[\"symbol\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)[expr_, args__] that takes the derivative of the expression given in expr corresponding to the operator specified in \!\(\*StyleBox[\"operator\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\). The objects \[Eta], \[Theta], x that appear in \!\(\*StyleBox[\"derivativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) are derivatives \[PartialD]/\[PartialD]\[Eta], \[PartialD]/\[PartialD]\[Theta], \[PartialD]/\[PartialD]x, while the objects \[Eta], \[Theta], x that appear in \!\(\*StyleBox[\"multiplicativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) are multiplied as given. The parameters args__ of the differential operator are given in the order \!\(\*StyleBox[\"derivativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"multiplicativeArgs\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\).";
dxsq::usage="dxsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SuperscriptBox[SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)], \(2\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Eta]dx\[Theta]::usage="d\[Eta]dx\[Theta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Theta]dx\[Eta]::usage="d\[Theta]dx\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
\[Theta]d\[Eta]::usage="\[Theta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Theta]d\[Eta]::usage="d\[Theta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Eta]d\[Eta]::usage="d\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
\[Eta]dx\[Eta]::usage="\[Eta]dx\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Eta]dxd\[Eta]::usage="d\[Eta]dxd\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Theta]dxd\[Eta]::usage="d\[Theta]dxd\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Theta]dxd\[Theta]::usage="d\[Theta]dxd\[Theta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Theta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Theta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Eta]xd\[Eta]::usage="d\[Eta]xd\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Theta]dx\[Theta]::usage="d\[Theta]dx\[Theta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
d\[Eta]dx\[Eta]::usage="d\[Eta]dx\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
boldxijsq::usage="boldxijsq[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the (\!\(\*SubscriptBox[\!\(\*StyleBox[\"x\",FontWeight->\"Bold\",FontSlant->\"Italic\"]\), \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) two-point building block from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
vij::Usage="vij[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the \!\(\*SubscriptBox[\(u\), \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) two-point building block from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
vMinusOneij::Usage="vij[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the \!\(\*SubscriptBox[\(u\), \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) two-point building block to the power -1 from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
yijsq::usage="yijsq[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the (\!\(\*SubscriptBox[\(y\), \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) two-point building block from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
ybijsq::usage="ybijsq[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the (\!\(\*SubscriptBox[OverscriptBox[\(y\), \(_\)], \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SuperscriptBox[\()\), \(2\)]\) two-point building block from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
SUSY2pf::usage="SUSY2pf[\!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"R\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the two-point function of an \[ScriptCapitalN]=2 superconformal primary of dimension \!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), spin \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and O(2) R-charge \!\(\*StyleBox[\"R\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
NonSUSY2pf::usage="NonSUSY2pf[\!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the two-point function of a conformal primary of dimension \!\(\*StyleBox[\"\[CapitalDelta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and spin \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
Reduction::usage="Reduction[\!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)]. Attempts to simplify \!\(\*StyleBox[\"expr\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\) by applying various reduction rules";
fastReduction::usage="fastReduction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Memoized version of Reduction";
fastApply::usage="fastApply[\!\(\*StyleBox[\"funct\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Acts with \!\(\*StyleBox[\"funct\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) on \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) and caches the result for each term in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) so as to improve performance on future calls";
Contraction::usage="Contraction[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Given an expression \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with some open indices it will return an index-free expression where the indices have been contracted";
Replace\[Eta]ToAny::usage="Replace\[Eta]ToAny[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Replaces every occurrence of \!\(\*SuperscriptBox[\(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\), \(\[Alpha]\)]\) in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with \!\(\*SuperscriptBox[\(\!\(\*StyleBox[\"any\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\), \(\[Alpha]\)]\), performing the appropriate index contractions";
Compare::usage="Compare[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*StyleBox[\"[\",FontWeight->\"Plain\",FontSlant->\"Italic\"]\)\!\(\*StyleBox[\",\",FontSlant->\"Italic\"]\)\!\(\*StyleBox[\" \",FontSlant->\"Italic\"]\)\!\(\*StyleBox[\"options\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)...]]. Solves the equation `\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) = 0` by replacing explicit numerical values to \!\(\*SubscriptBox[\(\[Eta]\), \(i\)]\) and \!\(\*SubscriptBox[\(x\), \(ij\)]\) and returns a set of replacement rules for the unknowns.\nThe options are:\n\texpReplacement \[Rule] {} set of numerical replacements for exponents (to help Solve finding a solution)\n\tverbose \[Rule] 0 print more information about the process\n\tvariables \[Rule] {\[ScriptCapitalA],...,\[ScriptCapitalZ]} unknowns to solve for\n\tminEqns \[Rule] -1 minimal number of numerical equations (-1 = as many as the unknowns)\n\twrapfunction \[Rule] Identity function to wrap the coefficients of the tensor structures to improve performance\n";
\[Eta]boldx\[Eta]ToThej::usage="\[Eta]boldx\[Eta]ToThej[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"J\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes (\!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) \!\(\*SubscriptBox[\!\(\*StyleBox[\"x\",FontWeight->\"Bold\",FontSlant->\"Italic\"]\), RowBox[{StyleBox[\"i\",FontFamily->\"Utopia\",FontSize->9,FontWeight->\"Plain\",FontSlant->\"Italic\"], \",\", StyleBox[\"j\",FontFamily->\"Utopia\",FontSize->9,FontWeight->\"Plain\",FontSlant->\"Italic\"]}]]\) \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SuperscriptBox[\()\), \(\!\(\*StyleBox[\"J\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
yijsqToTheQ::usage="yijsqToTheQ[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes (\!\(\*SubscriptBox[\(y\), \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SuperscriptBox[\()\), \(\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
ybijsqToTheQ::usage="ybijsqToTheQ[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes (\!\(\*SubscriptBox[OverscriptBox[\(y\), \(_\)], \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SuperscriptBox[\()\), \(\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
boldxijsqToTheQ::usage="boldxijsqToTheQ[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes (\!\(\*SuperscriptBox[SubscriptBox[\!\(\*StyleBox[\"x\",FontWeight->\"Bold\",FontSlant->\"Italic\"]\), \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)], \(2\)]\)\!\(\*SuperscriptBox[\()\), \(\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
vijToTheQ::usage="vijToTheQ[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes (\!\(\*SubscriptBox[\(v\), \(\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*SuperscriptBox[\()\), \(\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
SetToZero::usage="SetToZero[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"var\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] sets \!\(\*StyleBox[\"var\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) to zero in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\nSetToZero[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), {\!\(\*StyleBox[\"var1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"var2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), ...}] sets \!\(\*StyleBox[\"var1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"var2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), ... to zero in \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
ComplexConj::usage="ComplexConj[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the complex conjugate of \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
Nice::usage="Nice[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Shows \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in a more readable form (read only)";
FPower::usage="FPower[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\): Integer] multiplies \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) with itself \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) times\nFPower[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"around\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] raises \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) to the (non necessarily integer) power \!\(\*StyleBox[\"p\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) by expanding to order \!\(\*StyleBox[\"n\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) around \!\(\*StyleBox[\"around\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";


\[Eta]d\[Eta]::usage="\[Eta]d\[Eta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
\[Eta]d\[Theta]::usage="\[Eta]d\[Theta][\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";
\[Eta]d\[Theta]b::usage="\[Eta]d\[Theta]b[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Theta]b\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), synonym of \[Eta]d\[Theta]";
xdx::usage="xdx[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes \!\(\*StyleBox[\"x1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"x2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)";


fd\[Eta]dx\[Theta]::usage="fd\[Eta]dx\[Theta]. Memoized version of d\[Eta]dx\[Theta]";
f\[Eta]dx\[Theta]::usage="f\[Eta]dx\[Theta]. Memoized version of \[Eta]dx\[Theta]";
fd\[Theta]dx\[Eta]::usage="fd\[Theta]dx\[Eta]. Memoized version of d\[Theta]dx\[Eta]";
f\[Theta]d\[Eta]::usage="f\[Theta]d\[Eta]. Memoized version of \[Theta]d\[Eta]";
fd\[Theta]d\[Eta]::usage="fd\[Theta]d\[Eta]. Memoized version of d\[Theta]d\[Eta]";
fd\[Eta]d\[Eta]::usage="fd\[Eta]d\[Eta]. Memoized version of d\[Eta]d\[Eta]";
f\[Eta]dx\[Eta]::usage="f\[Eta]dx\[Eta]. Memoized version of \[Eta]dx\[Eta]";
fd\[Eta]dxd\[Eta]::usage="fd\[Eta]dxd\[Eta]. Memoized version of d\[Eta]dxd\[Eta]";
fd\[Theta]dxd\[Eta]::usage="fd\[Theta]dxd\[Eta]. Memoized version of d\[Theta]dxd\[Eta]";
fd\[Theta]dxd\[Theta]::usage="fd\[Theta]dxd\[Theta]. Memoized version of d\[Theta]dxd\[Theta]";
fd\[Eta]xd\[Eta]::usage="fd\[Eta]xd\[Eta]. Memoized version of d\[Eta]xd\[Eta]";
fd\[Theta]dx\[Theta]::usage="fd\[Theta]dx\[Theta]. Memoized version of d\[Theta]dx\[Theta]";
fd\[Eta]dx\[Eta]::usage="fd\[Eta]dx\[Eta]. Memoized version of d\[Eta]dx\[Eta]";
fdxsq::usage="fdxsq. Memoized version of dxsq";


d\[Eta]dx\[Theta]Mod::usage="";
d\[Theta]d\[Eta]Mod::usage="";


ChiralDm::usage="ChiralDm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the chiral derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)D \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which lowers the spin by 1/2";
ChiralDp::usage="ChiralDp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the chiral derivative \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)D \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which increases the spin by 1/2";
ChiralDbm::usage="ChiralDbm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the anti-chiral derivative \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*OverscriptBox[\(D\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which lowers the spin by 1/2";
ChiralDbp::usage="ChiralDbp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the anti-chiral derivative \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*OverscriptBox[\(D\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which increases the spin by 1/2";
ChiralDsq::usage="ChiralDsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the chiral derivative squared \!\(\*SuperscriptBox[\(D\), \(2\)]\)";
ChiralDbsq::usage="ChiralDbsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] computes the anti-chiral derivative squared \!\(\*SuperscriptBox[OverscriptBox[\(D\), \(_\)], \(2\)]\)";
ChiralQm::usage="ChiralQm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents the supercharge \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)Q \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which lowers the spin by 1/2";
ChiralQp::usage="ChiralQp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents the supercharge \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)Q \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which increases the spin by 1/2";
ChiralQbm::usage="ChiralQbm[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents the supercharge \!\(\*SubscriptBox[\(\[PartialD]\), \(\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\)\!\(\*OverscriptBox[\(Q\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which lowers the spin by 1/2";
ChiralQbp::usage="ChiralQbp[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents the supercharge \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\!\(\*OverscriptBox[\(Q\), \(_\)]\) \!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) which increases the spin by 1/2";
ChiralQsq::usage="ChiralQbsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents the supercharge squared \!\(\*SuperscriptBox[OverscriptBox[\(Q\), \(_\)], \(2\)]\)";
ChiralQbsq::usage="ChiralQsq[\!\(\*StyleBox[\"expr\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Theta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"x\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)] represents the supercharge squared \!\(\*SuperscriptBox[\(Q\), \(2\)]\)";
(**)
Chiral\[ScriptCapitalD]m::usage="Chiral\[ScriptCapitalD]m alias of ChiralQm. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";
Chiral\[ScriptCapitalD]p::usage="Chiral\[ScriptCapitalD]p alias of ChiralQp. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";
Chiral\[ScriptCapitalD]bm::usage="Chiral\[ScriptCapitalD]bm alias of ChiralQbm. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";
Chiral\[ScriptCapitalD]bp::usage="Chiral\[ScriptCapitalD]bp alias of ChiralQb. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";
Chiral\[ScriptCapitalQ]m::usage="Chiral\[ScriptCapitalQ]m alias of ChiralDm. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";
Chiral\[ScriptCapitalQ]p::usage="Chiral\[ScriptCapitalQ]p alias of ChiralDp. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";
Chiral\[ScriptCapitalQ]bm::usage="Chiral\[ScriptCapitalQ]bm alias of ChiralDbm. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";
Chiral\[ScriptCapitalQ]bp::usage="Chiral\[ScriptCapitalQ]bp alias of ChiralDp. It is meant to be acted on the reduced three-point function t(X,\[CapitalTheta],\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\))";


\[Eta]X\[Eta]::usage="\[Eta]X\[Eta][\!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the three-point building block \!\(\*StyleBox[\"\[Eta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)X\!\(\*StyleBox[\"\[Eta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[Eta]X\[CapitalTheta]b::usage="\[Eta]X\[CapitalTheta]b[\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the three-point building block \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)X\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[Eta]X\[CapitalTheta]::usage="\[Eta]X\[CapitalTheta][\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the three-point building block \!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)X\[CapitalTheta] from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[CapitalTheta]X\[CapitalTheta]b::usage="\[CapitalTheta]X\[CapitalTheta]b[]. Computes the three-point building block \[CapitalTheta]X\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[CapitalTheta]\[Eta]::usage="\[CapitalTheta]\[Eta][\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the three-point building block \[CapitalTheta]\[Eta] from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[CapitalTheta]b\[Eta]::usage="\[CapitalTheta]b\[Eta][\!\(\*StyleBox[\"\[Eta]\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the three-point building block \!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\)\[Eta] from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[CapitalTheta]\[CapitalTheta]b::usage="\[CapitalTheta]\[CapitalTheta]b[]. Computes the three-point building block \[CapitalTheta]\!\(\*OverscriptBox[\(\[CapitalTheta]\), \(_\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[CapitalTheta]sq::usage="\[CapitalTheta]sq[]. Computes the three-point building block \!\(\*SuperscriptBox[\(\[CapitalTheta]\), \(2\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[CapitalTheta]bsq::usage="\[CapitalTheta]bsq[]. Computes the three-point building block \!\(\*SuperscriptBox[OverscriptBox[\(\[CapitalTheta]\), \(_\)], \(2\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
Xsq::usage="Xsq[]. Computes the three-point building block \!\(\*SuperscriptBox[\(X\), \(2\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
XsqToTheQ::usage="XsqToTheQ[\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the three-point building block (\!\(\*SuperscriptBox[\(X\), \(2\)]\)\!\(\*SuperscriptBox[\()\), \(\!\(\*StyleBox[\"Q\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)\)]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[Eta]X\[Eta]ToTheJ::usage="\[Eta]X\[Eta]ToTheJ[\!\(\*StyleBox[\"\[Eta]1\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\), \!\(\*StyleBox[\"\[Eta]2\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)][\!\(\*StyleBox[\"J\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)]. Computes the three-point building block (\!\(\*StyleBox[\"\[Eta]1\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)X\!\(\*StyleBox[\"\[Eta]2\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]\)\!\(\*SuperscriptBox[\")\", StyleBox[\"J\",FontFamily->\"Utopia\",FontSlant->\"Italic\"]]\) from \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
kinPrefactor::usage="kinPrefactor[{\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"R1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}, {\!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"R2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}]. Computes the kinematic prefactor for a three-point function with the first two operators having quantum numbers (\!\(\*StyleBox[\"\[CapitalDelta]1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"R1\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) and (\!\(\*StyleBox[\"\[CapitalDelta]2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"j2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\), \!\(\*StyleBox[\"R2\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)) using the three-point function formalism of \!\(\*TemplateBox[{RowBox[{\"[\", \"1503.04961\", \"]\"}], {URL[\"https://arxiv.org/abs/1503.04961\"], None}, \"https://arxiv.org/abs/1503.04961\", \"HyperlinkActionRecycled\", {\"HyperlinkActive\"}, BaseStyle -> {\"Hyperlink\"}, HyperlinkAction -> \"Recycled\"},\"HyperlinkTemplate\"]\)";
\[Eta]Replacement::usage="\[Eta]Replacement[\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)]. Computes the replacement that should be done to \[Eta]\!\(\*StyleBox[\"i\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\) in the prefactor when expanding the reduced three-point function into a full 3d expression";


tokeepSet::usage="tokeepSet[{\!\(\*StyleBox[\"list\",\nFontFamily->\"Utopia\",\nFontSlant->\"Italic\"]\)}]. Sets the list of variables to keep in the computations (the other ones will be set to zero)";
orderexp::usage="orderexp. Sets the maximal order to which one should expand the powers";


(* ::Subsection::Closed:: *)
(*Some preparatory definitions*)


(*List of symbols that should be treated as constants*)
ConstantsList={\[ScriptCapitalA],\[ScriptCapitalB],\[ScriptCapitalC],\[ScriptCapitalD],\[ScriptCapitalE],\[ScriptCapitalF],\[ScriptCapitalG],\[ScriptCapitalH],\[ScriptCapitalI],\[ScriptCapitalJ],\[ScriptCapitalK],\[ScriptCapitalL],\[ScriptCapitalM],\[ScriptCapitalN],\[ScriptCapitalO],\[ScriptCapitalP],\[ScriptCapitalQ],\[ScriptCapitalR],\[ScriptCapitalS],\[ScriptCapitalT],\[ScriptCapitalU],\[ScriptCapitalV],\[ScriptCapitalW],\[ScriptCapitalX],\[ScriptCapitalY],\[ScriptCapitalZ]};
Format[\[ScriptCapitalC][subscriptn_]]:=Subscript[\[ScriptCapitalC], subscriptn];
(*Protected symbols used in the internal notation*)
Protect[\[Gamma],\[Epsilon]\[Mu]\[Nu]\[Rho],\[Mu],\[Nu],\[Rho],\[CurlyEpsilon]];
(*I put these symbols here so that they are not generated with the context SpinorAlgebra`Private` in front*)


(*Can be specified so that it doesn't have to check which \[Theta]s or xs appear in an expression, if it's always the same ones.*)
working\[Theta]s = {};
workingxs = {};


(* ::Subsection::Closed:: *)
(*Begin `Private`*)


(*Begin["`Private`"];*)


(* ::Section::Closed:: *)
(*Initial definitions*)


(* ::Subsection::Closed:: *)
(*Basic definitions*)


toString[x_]:=ToString[x,InputForm];
toStringNoContext[x_]:=StringReplace[ToString[x,InputForm],"SpinorAlgebra`"->""];


(*Definition of fermionic number*)
(*Fermionic variables are \[Theta]n, where n is a non negative integer*)
FN[x_NonCommutativeMultiply]:=Mod[Plus@@(FN/@List@@x),2];
FN[x_d]:=Mod[Plus@@(FN/@List@@x),2];
FN[x_Symbol]:=Mod[Plus@@StringCases[SymbolName[x],{
    "\[Theta]"~~(DigitCharacter..)->1,
    "\[Theta]b"~~(DigitCharacter..)->1,
    "\[Theta]"~~(DigitCharacter..)~~"sq"->1,
    "\[Theta]b"~~(DigitCharacter..)~~"sq"->1},Overlaps->True],2];
FN[x_Real|x_Complex|x_Rational|x_Integer]:=0;
FN[a_+b_]:=If[FN[a]==FN[b],FN[a],Message[FN::FNfail,a+b];Inactive[FN][a+b],Message[FN::FNfail,a+b];Inactive[FN][a+b]];
FN[]:=0;
FN[a_,b__]:=Mod[FN[a]+FN[b],2];
FN[a_^b_]:=0;
FN[x_Times]:=Mod[Plus@@(FN/@List@@x),2];
FN[up[H_][a_]]:=FN[H];
FN[low[H_][a_]]:=FN[H];
FN[v[H_][a_]]:=0;
FN[\[Gamma][m_][a__]]:=0;
FN[\[Gamma][m_]]:=0;
FN[\[CurlyEpsilon][m__]]:=0;
FN[\[Delta][m__]]:=0;
FN[\[Epsilon]3[m__]]:=0;
FN[\[ScriptG][\[Alpha]_,\[Beta]_]]:=0;
FN[x_?xvQ]:=0;


FN::FNfail = "The expression `1` has undefined fermion number."


(*Definition of the canonical ordering*)
chosenOrdering=OrderedQ[{#1,#2}]&;              (*you can also choose a customized ordering*)
FSort[a_List]:=Sort[a,chosenOrdering];
FSignature[a_List]:=Signature[PermutationList[FindPermutation[
    Select[a,FN[#]==1&],
    Select[FSort[a],FN[#]==1&]]]];


ConstQ[x_?NumericQ]:=True;
ConstQ[x_Plus]:=And@@(ConstQ[#]&/@List@@x);
ConstQ[x_Times]:=And@@(ConstQ[#]&/@List@@x);
ConstQ[x_^a_]:=ConstQ[x]&&ConstQ[a];
ToExpression[
    StringJoin@@Table[
        toString[con]<>"/: ConstQ["<>toString[con]<>"] := True; ",
    {con,ConstantsList}]];
ConstQ[CFTs4D`\[Lambda]M[x_]]:= True;
ConstQ[CFTs4D`\[Lambda]P[x_]]:= True;
ConstQ[CFTs4D`\[Lambda][x_]] := True;

(*Use ConstQ to know whether a symbol is considered a constant or not*)


makeConst[s_,symbols__]:=makeConst/@{s,symbols};
makeConst[symbol_Symbol]:=(symbol/:ConstQ[symbol]:=True);
makeConst[symbol_]:=(ConstQ[symbol]:=True)


(* ::Subsection::Closed:: *)
(*NonCommutativeMultiply*)


(*Properties and ordering of NonCommutativeMultiply*)
Unprotect[NonCommutativeMultiply];
ClearAttributes[NonCommutativeMultiply,Flat];

NonCommutativeMultiply[a___,c_?ConstQ b_,e___]:=c*NonCommutativeMultiply[a,b,e];
NonCommutativeMultiply[a___,c_Plus,b___]:=Plus@@(NonCommutativeMultiply[a,#,b]&/@(List@@c));
NonCommutativeMultiply[a___,c_?ConstQ,b___]:=c*NonCommutativeMultiply[a,b];
NonCommutativeMultiply[a___,c_Power,b___]:=c*NonCommutativeMultiply[a,b];

NonCommutativeMultiply[a___,b_,c___]/;(FN[b]==0):=b*a**c;
NonCommutativeMultiply[a___]/;(Length[{a}]===1):=a;
NonCommutativeMultiply[a___]/;(Length[{a}]===0):=1;

NonCommutativeMultiply[a___,b_*c_,e___]:=NonCommutativeMultiply[a,b,c,e];

(*If I insert this rule the numerical values comparison fails... Find out what goes wrong.*)
(*NonCommutativeMultiply[a___]/;(!OrderedQ[{a},chosenOrdering]):=FSignature[{a}]*NonCommutativeMultiply@@FSort[{a}];*)

NonCommutativeMultiply[a___,b_?ConstQ,c___]:=b a**c;

SetAttributes[NonCommutativeMultiply,Flat];
Protect[NonCommutativeMultiply];


(*Reorders the factor of a ** product*)
ReorderNCM[expr_]:=expr/.NonCommutativeMultiply->tempNCM/.tempNCM[a___]:>FSignature[{a}]*NonCommutativeMultiply@@FSort[{a}]/.tempNCM->NonCommutativeMultiply


(* ::Section::Closed:: *)
(*From user form to compact form*)


(* ::Subsection::Closed:: *)
(*Useful auxiliary functions*)


(*Recognizes the type of variables in the monomials*)
\[Theta]Q[x_]:=StringMatchQ[toString[x],"\[Theta]"~~(DigitCharacter..)];
\[Theta]bQ[x_]:=StringMatchQ[toString[x],"\[Theta]b"~~(DigitCharacter..)];
\[Theta]\[Theta]bQ[x_]:=\[Theta]Q[x]||\[Theta]bQ[x];
\[Eta]Q[x_]:=StringMatchQ[toString[x],"\[Eta]"~~(DigitCharacter..)];
\[Chi]Q[x_]:=StringMatchQ[toString[x],"\[Chi]"~~(DigitCharacter..)];
spinorQ[x_]:=\[Eta]Q[x]||\[Theta]Q[x]||\[Theta]bQ[x]||\[Chi]Q[x];
xQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)];
xvQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)~~"v"~~__];
sQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)];
svQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)~~"v"~~__];
sqQ[x_]:=StringMatchQ[toString[x],__~~"sq"];
\[Theta]sqQ[x_]:=StringMatchQ[toString[x],"\[Theta]"~~(DigitCharacter..)~~"sq"];
\[Theta]bsqQ[x_]:=StringMatchQ[toString[x],"\[Theta]b"~~(DigitCharacter..)~~"sq"];
\[Theta]\[Theta]bsqQ[x_]:=StringMatchQ[toString[x],"\[Theta]"|"\[Theta]b"~~(DigitCharacter..)~~"sq"];
xxQ[x_]:=StringMatchQ[toString[x],"x"~~(DigitCharacter..)~~"x"~~(DigitCharacter..)];
xsQ[x_]:=StringMatchQ[toString[x],("s"~~(DigitCharacter..)~~"x"~~(DigitCharacter..))|("x"~~(DigitCharacter..)~~"s"~~(DigitCharacter..))];
ssQ[x_]:=StringMatchQ[toString[x],"s"~~(DigitCharacter..)~~"s"~~(DigitCharacter..)];
xsqQ[x_]:=StringMatchQ[toString[x],"x"~~DigitCharacter..~~"sq"];


StringMatchQ["x12v","x"~~(DigitCharacter..)]


(*Some useful auxiliary functions*)
sq[x_]:=ToExpression[toString[x]<>"sq"];
glue[x___]:= ToExpression[StringTrim[StringJoin@@(ToString/@{x})," "]];
lorentzd[x_?xvQ,y_?xvQ]/;(x[[1]]===y[[1]]):=If[x===y,ToExpression[StringDrop[toString[Head[x]],-1]<>"sq"],
                                               ToExpression[StringDrop[toString[Head[x]],-1]<>StringDrop[toString[Head[y]],-1]]];
lorentzd[x_?xQ,y_?xQ]:=If[x===y,sq[x],glue[x,y]];
bar[x_?\[Theta]Q]:=ToExpression[StringReplace[toString[x],a_~~(b:DigitCharacter..):>a<>"b"<>b]];
unbar[x_?\[Theta]bQ]:=ToExpression[StringReplace[toString[x],a_~~"b"~~(b:DigitCharacter..):>a<>b]];


(*Sets some variables to zero*)
SetToZero[expr_,var_?\[Eta]\[Eta]bQ]:=expr/.var->0/.d[a___,0,b___]:>0;
SetToZero[expr_,var_?\[Theta]\[Theta]bQ]:=expr/.{var->0,sq[var]->0}/.d[a___,0,b___]:>0;
SetToZero[expr_,var_?xQ]:=expr/.{var->0,sq[var]->0,
                          ToExpression[toString[var]<>"v"][\[Mu]_]-> 0,
                          x_?(StringMatchQ[toString[#],toString[var]~~"x"~~(DigitCharacter..)]&):> 0,
                          x_?(StringMatchQ[toString[#],"x"~~(DigitCharacter..)~~toString[var]]&):> 0}/.d[a___,0,b___]:>0;
SetToZero[expr_,var_List]:=Fold[SetToZero,expr,var];


(*SetToZero\[Theta]sq[expr_,{var__?\[Theta]\[Theta]bQ}]:=Module[{expanded,lis},lis=If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}];
                                         Sum[If[Or@@(#>=2&/@(Exponent[\[Theta]\[Theta]bcount[el]/.x_?\[Theta]\[Theta]bQ/;!MemberQ[{var},x]:>1,#]&/@{var})),0,el],{el,lis}]
                                 ];
SetToZero\[Theta]sq[expr_,var_?\[Theta]\[Theta]bQ]:=SetToZero\[Theta]sq[expr,{var}];*)


listwise[f_][expr_]:=Module[{Expr,expanded},
    If[Head[expanded=Expand[expr]]===Plus,
        Expr=List@@expanded,
        Expr={expanded}];
    WriteString["stdout","\nThere are "<>ToString[Length[Expr]]<>" terms.\n"];
    
    Return[Plus@@(f/@Expr)]
];
listwise[f_,print_][expr_]:=listwise[Function[{\[ScriptX]},WriteString["stdout",print];f[\[ScriptX]]]][expr]


(* ::Subsection::Closed:: *)
(*Conversion of the notations and operator d*)


(*Splits monomials into contractions made by the operator d. Henceforth referred to as a d-expression*)
SplitMonomials[m_NonCommutativeMultiply]:=NonCommutativeMultiply@@(SplitMonomials/@ List@@m);
SplitMonomials[m_Times]:=Times@@(SplitMonomials/@ List@@m);
SplitMonomials[s_Plus]:=Plus@@(SplitMonomials/@ List@@s);
SplitMonomials[x_?ConstQ]:=x;
SplitMonomials[Power[x_,p_]]:=Power[SplitMonomials[x],p];

SplitMonomials[s_Symbol]:=Module[{str=SymbolName[s],int},
    int=StringReplace[str,
        {"x"~~(y:DigitCharacter..)~~(z:"\[Mu]"|"\[Nu]"|"\[Rho]"):>  "x"<>y<>"v["<>z<>"]|",
        "x"~~(y:DigitCharacter..)~~"sq":>"x"<>y<>"sq|",
        "x"~~(y:DigitCharacter..):> "x"<>y<>"|",
        "\[Eta]"~~(y:DigitCharacter..):> "\[Eta]"<>y<>"|",
        "\[Theta]"~~(y:DigitCharacter..)~~"sq":>"\[Theta]"<>y<>"sq|",
        "\[Theta]b"~~(y:DigitCharacter..)~~"sq":>"\[Theta]b"<>y<>"sq|",
        "\[Theta]"~~(y:DigitCharacter..):> "\[Theta]"<>y<>"|",
        "\[Theta]b"~~(y:DigitCharacter..):> "\[Theta]b"<>y<>"|",
        "\[Gamma]"~~(z:"\[Mu]"|"\[Nu]"|"\[Rho]"):> "\[Gamma]["<>z<>"]|",
        "\[Epsilon]\[Mu]\[Nu]\[Rho]":> "\[Epsilon]\[Mu]\[Nu]\[Rho]|"
    }];
    Return[(d@@ToExpression/@StringSplit[int,"|"])]
];


(*Writes monomials back to the user form*)
WriteMonomials[m_NonCommutativeMultiply]:=NonCommutativeMultiply@@(WriteMonomials/@ List@@m);
WriteMonomials[m_Times]:=Times@@(WriteMonomials/@ List@@m);
WriteMonomials[s_Plus]:=Plus@@(WriteMonomials/@ List@@s);
WriteMonomials[x_?ConstQ]:=x;
WriteMonomials[Power[x_,p_]]:=Power[WriteMonomials[x],p];
WriteMonomials[x_Symbol]:=x;
WriteMonomials[x_List]:=x;

WriteMonomials[x_d]:=Module[{rule,tempglue,ind},
    ind=x/.{d[a_[m_],b_[n_],H1_,\[Sigma][m_,n_],H2_]:>{Rule[m,\[Mu]],Rule[n,\[Nu]]},d[a_[m_],b_[n_],H1_,\[Sigma]b[m_,n_],H2_]:>{Rule[m,\[Mu]],Rule[n,\[Nu]]},_->{}};

    rule={\[Gamma][m_]:>ToExpression["\[Gamma]"<>toString[m]],
    a_?xvQ:>StringTake[toString[a],{1,StringPosition[toString[a],"v"][[1,1]]-1}]<>toStringNoContext[First@a]};

    (tempglue@@(x/.rule))/.tempglue->glue
    
];


(*Linearity over constants*)
d[a___,n_*b_,c___]/;ConstQ[n]:=n*d[a,b,c];
d[a___,b1_+b2_,c___]:=d[a,b1,c]+d[a,b2,c];

(*Enforces the ordering of the operator d*)
\[Gamma]Q[x_]:=StringMatchQ[toString[Head[x]],"\[Gamma]"];
(*Sorts \[Eta]i\[Theta]j, \[Eta]i\[Eta]j and \[Theta]i\[Theta]j. Imposes \[Theta]\[Theta] \[Rule] \[Theta]sq, \[Eta]\[Eta] \[Rule] 0*)
d[x_?\[Theta]\[Theta]bQ,x_?\[Theta]\[Theta]bQ]:=sq[x];
d[x_?\[Theta]\[Theta]bQ,y_?\[Theta]\[Theta]bQ]/;(!chosenOrdering[x,y]):=d[y,x];
d[x_?\[Eta]Q,x_?\[Eta]Q]:=0;
d[x_?\[Eta]Q,y_?\[Eta]Q]/;(!chosenOrdering[x,y]):=-d[y,x];
d[x_?\[Eta]Q,y_?\[Theta]\[Theta]bQ]/;(!chosenOrdering[x,y]):=-d[y,x];
d[x_?\[Theta]\[Theta]bQ,y_?\[Eta]Q]/;(!chosenOrdering[x,y]):=-d[y,x];

(*Sets to zero \[Theta]x\[Theta] and \[Theta]bx\[Theta]b with the same \[Theta]*)
d[x_?\[Theta]\[Theta]bQ,y_?xQ,x_?\[Theta]\[Theta]bQ]:=0;

(*Sorts monomials with three terms of the form \[Eta]x\[Eta], \[Theta]x\[Theta]b, \[Eta]x\[Theta]b, \[Theta]x\[Eta]*)
d[x_?\[Theta]bQ,y_?xQ,z_?\[Theta]Q]:=-d[z,y,x];
d[x_?\[Theta]bQ,y_?xQ,z_?\[Eta]Q]:=d[z,y,x];
d[x_?\[Eta]Q,y_?xQ,z_?\[Eta]Q]/;(!chosenOrdering[x,z]):=d[z,y,x];
d[x_?\[Theta]Q,y_?xQ,z_?\[Theta]Q]/;x=!=z&&(!chosenOrdering[x,z]):=-d[z,y,x];
d[x_?\[Theta]bQ,y_?xQ,z_?\[Theta]bQ]/;x=!=z&&(!chosenOrdering[x,z]):=-d[z,y,x];
d[x_?\[Eta]Q,y_?xQ,z_?\[Theta]Q]:=d[z,y,x];

(*Puts \[Epsilon]\[Mu]\[Nu]\[Rho] at the beginning. Puts to zero \[Epsilon]\[Mu]\[Nu]\[Rho] contracted with equal xs. Orders canonically the xs contracted with \[Epsilon]\[Mu]\[Nu]\[Rho].*)
d[x__,\[Epsilon]\[Mu]\[Nu]\[Rho],y___]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho],x,y];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],y___]/;(!DuplicateFreeQ[Head/@Select[{y},xvQ[#]&]]):=0;
d[\[Epsilon]\[Mu]\[Nu]\[Rho],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(!OrderedQ[Head/@{x}])):=Module[{sign,hlist},
    hlist=SortBy[{x},Head];
    sign=Signature[First/@hlist];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho],Sequence@@(hlist/.Thread[Rule[First/@hlist,{\[Mu],\[Nu],\[Rho]}]])]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],x__?xvQ]/;(DuplicateFreeQ[Head/@{x}]&&(OrderedQ[Head/@{x}])&&(First/@{x}=!={\[Mu],\[Nu],\[Rho]})):=Module[{sign},
    sign=Signature[First/@{x}];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Mu],\[Nu],\[Rho]}]])]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a___,x_?\[Theta]\[Theta]bQ|x_?\[Eta]Q|x_?\[Gamma]Q,y_?xvQ,b___]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,y,x,b]; (*Check this*)
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]bQ,y_?\[Gamma]Q,z_?\[Theta]Q]:=-d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]bQ,y_?\[Gamma]Q,z_?\[Eta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Eta]Q,y_?\[Gamma]Q,z_?\[Eta]Q]/;(!chosenOrdering[x,z]):=d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]\[Theta]bQ,y_?\[Gamma]Q,x_?\[Theta]\[Theta]bQ]:=0;
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]Q,y_?\[Gamma]Q,z_?\[Theta]Q]/;x=!=z&&(!chosenOrdering[x,z]):=-d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]bQ,y_?\[Gamma]Q,z_?\[Theta]bQ]/;x=!=z&&(!chosenOrdering[x,z]):=-d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Eta]Q,y_?\[Gamma]Q,z_?\[Theta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],x__?xvQ,\[Eta]_,s_?\[Gamma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]=!={\[Mu],\[Nu],\[Rho]})):=Module[{sign},
    sign=Signature[First/@Append[{x},s]];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho],Sequence@@({x}/.Thread[Rule[First/@{x},{\[Mu],\[Nu]}]]),\[Eta],\[Gamma][\[Rho]],\[Eta]b]
];
d[\[Epsilon]\[Mu]\[Nu]\[Rho],x__?xvQ,\[Eta]_,s_?\[Gamma]Q,\[Eta]b_]/;(DuplicateFreeQ[Head/@{x}]&&(First/@Append[{x},s]==={\[Mu],\[Nu],\[Rho]})&&!OrderedQ[Head/@{x}]):=Module[{sign},
    sign=Signature[Head/@{x}];
    sign*d[\[Epsilon]\[Mu]\[Nu]\[Rho],Sequence@@({x}/.Thread[Rule[Head/@{x},Sort[Head/@{x}]]]),\[Eta],\[Gamma][\[Rho]],\[Eta]b]
];


(*d[X.Y]=XY*)
d[x_?sqQ]:=x;
d[x_?xQ,x_?xQ]:=sq[x];
d[x_?xQ,y_?xQ]:=glue[x,y];


(*Momentarily makes d consider \[Theta] and \[Theta]b as commuting*)
(*This is needed for the computation with explicit indices where all signs are put in the end*)
hold[d][x_?\[Theta]\[Theta]bQ,y_?\[Theta]\[Theta]bQ]/;(!chosenOrdering[x,y]):=-d[y,x];
hold[d][x_?\[Theta]bQ,y_?xQ,z_?\[Theta]Q]:=d[z,y,x];
hold[d][x_?\[Theta]Q,y_?xQ,z_?\[Theta]Q]/;x=!=z&&(!chosenOrdering[x,z]):=d[z,y,x];
hold[d][x_?\[Theta]bQ,y_?xQ,z_?\[Theta]bQ]/;x=!=z&&(!chosenOrdering[x,z]):=d[z,y,x];
hold[d][\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]bQ,y_?\[Gamma]Q,z_?\[Theta]Q]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
hold[d][\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]Q,y_?\[Gamma]Q,z_?\[Theta]Q]/;x=!=z&&(!chosenOrdering[x,z]):=d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
hold[d][\[Epsilon]\[Mu]\[Nu]\[Rho],a__?xvQ,x_?\[Theta]bQ,y_?\[Gamma]Q,z_?\[Theta]bQ]/;x=!=z&&(!chosenOrdering[x,z]):=d[\[Epsilon]\[Mu]\[Nu]\[Rho],a,z,\[Gamma]@@y,x];
hold[d][x__]:=d[x]


(*Operator on functions that transforms the input in internal notation, applies the function, and transfrorms back in user form.*)
usr[f_]:=WriteMonomials[f[SplitMonomials[#],##2]]&;
uusr[f_]:=WriteMonomials[f[SplitMonomials[#1],SplitMonomials[#2],##3]]&;


(* ::Subsection::Closed:: *)
(*Other useful functions that use the definitions of d*)


(*Counts \[Theta]s and \[Theta]bs in a term. Acts on d-expressions*)
\[Theta]count[expr_]:=Module[{tempNCM},
               Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]sqQ:>ToExpression[StringDrop[toString[x],-2]]^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
               ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]Q|(_?\[Theta]Q)^p_]}]/.List->Times;
\[Theta]bcount[expr_]:=Module[{tempNCM},
                Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]bsqQ:>ToExpression[StringDrop[toString[x],-2]]^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
                ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]bQ|(_?\[Theta]bQ)^p_]}]/.List->Times;
\[Theta]\[Theta]bcount[expr_]:=Module[{tempNCM},
                Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]\[Theta]bsqQ:>ToExpression[StringDrop[toString[x],-2]]^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
                ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]\[Theta]bQ|(_?\[Theta]\[Theta]bQ)^p_]}]/.List->Times;
(*Doesn't distinguish between different \[Theta]s and gives directly the number*)
All\[Theta]count[expr_]:=Module[{tempNCM,\[Theta]},
               Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]sqQ:>\[Theta]0^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
               ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]Q|(_?\[Theta]Q)^p_]}/.{x_?\[Theta]Q->\[Theta]}/.List->Times//Exponent[#,\[Theta]]&];
All\[Theta]bcount[expr_]:=Module[{tempNCM,\[Theta]b},
                Flatten[tempNCM[expr]/.(a_**b_)^p_:>(a^p)**(b^p)/.d[a__]^p_:>d@@(#^p &/@{a})/.x_?\[Theta]bsqQ:>\[Theta]b0^2/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM}
                ,Infinity,tempNCM]/.{tempNCM[seq__] :> Cases[PowerExpand/@{seq},_?\[Theta]bQ|(_?\[Theta]bQ)^p_]}/.{x_?\[Theta]bQ->\[Theta]b}/.List->Times//Exponent[#,\[Theta]b]&];
(*Returns True if there are more than 2 equal \[Theta]'s or \[Theta]b's*)
tooMany\[Theta]Q[expr_]:=With[{\[Theta]countsaved = \[Theta]count[expr]},!And@@(#<=2 & /@ Exponent[\[Theta]countsaved,Variables[\[Theta]countsaved]])];
tooMany\[Theta]bQ[expr_]:=With[{\[Theta]bcountsaved = \[Theta]bcount[expr]},!And@@(#<=2 & /@ Exponent[\[Theta]bcountsaved,Variables[\[Theta]bcountsaved]])];
(*Tells whether a term is completely bosonic or not (i.e. if it contains \[Theta]s and \[Theta]bs at all*)
\[Theta]FreeQ[x_]:=All\[Theta]count[x]+All\[Theta]bcount[x]==0;


ComplexConj[expr_]:=Module[{tempNCM,tempd},expr/.{Complex[a_,b_]:>Complex[a,-b]
                       }/.{NonCommutativeMultiply:>tempNCM
                       }/.{tempNCM[a__]:>Reverse[tempNCM[a]]
                       }/.{d[x__]:>d[x] If[All\[Theta]count[d[x]]+All\[Theta]bcount[d[x]]==2,-1,1]
                       }/.{a_?\[Theta]bQ:>unbar[a],b_?\[Theta]Q:>bar[b],
                           c_?\[Theta]sqQ:>-ToExpression[StringReplace[toString[c],"\[Theta]"->"\[Theta]b"]],
                           c_?\[Theta]bsqQ:>-ToExpression[StringReplace[toString[c],"\[Theta]b"->"\[Theta]"]]
                       }/.{tempNCM->NonCommutativeMultiply}];


(*Operator on functions that transforms the input in internal notation, applies the function, and transfrorms back in user form.*)
u[f_]:=WriteMonomials[f[SplitMonomials[#],##2]]&;
uu[f_]:=WriteMonomials[f[SplitMonomials[#1],SplitMonomials[#2],##3]]&;


(* ::Subsection::Closed:: *)
(*Display notation in a nice form*)


Nice[expr_]:=expr/.{d->dNice,a_Symbol?xxQ:>dNice[a],a_Symbol?sqQ:>dNice[a]}/.NonCommutativeMultiply->NiceNCM;
UnNice[expr_]:=expr/.dNice->d/.NiceNCM->NonCommutativeMultiply


Format[dNice[a_?sqQ]]:=StringCases[toString[a],{(x:"\[Theta]"|"x")~~(y:DigitCharacter..)~~"sq":> Subscript[x, y],"\[Theta]b"~~(y:DigitCharacter..)~~"sq":> Subscript["\!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\)", y]}][[1]]^2;
Format[dNice[a_?xxQ]]:=StringCases[toString[a],{"x"~~(y:DigitCharacter..)~~"x"~~(z:DigitCharacter..):> Row[{Subscript["x", y],"\[NegativeThinSpace]\[CenterDot]\[NegativeThinSpace]",Subscript["x", z]}]}][[1]];
Format[dNice[a__]]:=Row[{"\[ThinSpace]",a,"\[ThinSpace]"}/.{\[Gamma][\[Rho]]->"\!\(\*SubscriptBox[\(\[Gamma]\), \(\[Rho]\)]\)",
                              \[Gamma][\[Mu]]->"\!\(\*SubscriptBox[\(\[Gamma]\), \(\[Mu]\)]\)",
                              x_?xvQ:>StringCases[toString[x],"x"~~(y:DigitCharacter..)~~"v["~~z_~~"]":>  Subscript["x",y]^z][[1]],
                              x_?\[Eta]Q:>StringCases[toString[x],{"\[Eta]"~~(z:DigitCharacter..):>Subscript["\[Eta]", z]}][[1]],
                              x_?\[Theta]\[Theta]bQ:>StringCases[toString[x],{"\[Theta]"~~(z:DigitCharacter..):>Subscript["\[Theta]", z],"\[Theta]b"~~(z:DigitCharacter..):>Subscript["\!\(\*OverscriptBox[\(\[Theta]\), \(_\)]\)", z]}][[1]],
                              x_?xQ:>StringCases[toString[x],{"x"~~(y:DigitCharacter..):>Subscript["x", y]}][[1]],
                              \[Epsilon]\[Mu]\[Nu]\[Rho] -> "\!\(\*SubscriptBox[\(\[CurlyEpsilon]\), \(\[Mu]\[Nu]\[Rho]\)]\)" 
                              }];
Format[NiceNCM[a__]]:=Infix[NiceNCM[a],""];


(* ::Section::Closed:: *)
(*Reduction of expressions*)


(* ::Subsection::Closed:: *)
(*Algebra of \[Gamma] matrices*)


SetAttributes[\[ScriptG],Orderless];
SetAttributes[\[Delta],Orderless];
\[ScriptG]/:ConstQ[\[ScriptG]]:=True; (*\[ScriptG] is the 4 dimensional metric tensor*)
Unprotect[\[CurlyEpsilon]];\[CurlyEpsilon]/:ConstQ[\[CurlyEpsilon]]:=True;Protect[\[CurlyEpsilon]];(*\[CurlyEpsilon] is the 2 dimensional \[Epsilon]-tensor*)
\[Epsilon]3/:ConstQ[\[Epsilon]3]:=True; (*\[Epsilon]3 is the 3 dimensional \[Epsilon]-tensor*)
\[Delta]/:ConstQ[\[Delta]]:=True; (*\[Delta] is the 2 dimensional Kronecker \[Delta]*)
(*Rules to carry out the Pauli algebra*)
PauliRulesgen={
            \[Delta][\[Alpha]_,\[Beta]_]ANY_[i___,\[Beta]_,f___]:>ANY[i,\[Alpha],f],
			(*\[ScriptG][\[Mu]_,\[Nu]_]ANY_[i___,\[Nu]_,f___]:>ANY[i,\[Mu],f],*)
			\[ScriptG][\[Mu]_,\[Nu]_]\[Gamma][\[Nu]_][aa_,bb_]:>\[Gamma][\[Mu]][aa,bb],
            \[ScriptG][\[Mu]_,\[Mu]_]->3,\[ScriptG][\[Mu]_,\[Nu]_]^2->3,
            \[Delta][\[Alpha]_,\[Alpha]_]->2,\[Delta][\[Alpha]_,\[Beta]_]^2->2,
            \[ScriptG][\[Mu]_,\[Nu]_]\[ScriptG][\[Mu]_,\[Rho]_]:>\[ScriptG][\[Nu],\[Rho]],
            \[Delta][\[Mu]_,\[Nu]_]\[Delta][\[Mu]_,\[Rho]_]:>\[Delta][\[Nu],\[Rho]],
            \[CurlyEpsilon][x:OrderlessPatternSequence[\[Alpha]_,\[Beta]_]]\[CurlyEpsilon][y:OrderlessPatternSequence[\[Beta]_,\[Gamma]_]] :> If[{x}[[2]]===\[Beta],1,-1]*If[{y}[[1]]===\[Beta],1,-1]*\[Delta][\[Alpha],\[Gamma]],
            \[CurlyEpsilon][\[Alpha]_,\[Beta]_]^2:>-2,
            \[Epsilon]3[a__]/;!DuplicateFreeQ[{a}]:>0,
		    \[CurlyEpsilon][\[Alpha]_,\[Beta]_]up[X_][\[Beta]_]:>low[X][\[Alpha]],
			\[CurlyEpsilon][\[Alpha]_,\[Beta]_]low[X_][\[Beta]_]:>up[X][\[Alpha]],
			\[CurlyEpsilon][\[Beta]_,\[Alpha]_]up[X_][\[Beta]_]:>-low[X][\[Alpha]],
			\[CurlyEpsilon][\[Beta]_,\[Alpha]_]low[X_][\[Beta]_]:>-up[X][\[Alpha]],
            \[ScriptG][\[Mu]_,\[Nu]_]v[x_][\[Nu]_] :> v[x][\[Mu]]
            };

(*\[Gamma] has lower \[Alpha]\[Beta] indices*)
PauliRed  :={
            \[Gamma][\[Mu]_][\[Alpha]_,\[Beta]_]\[Gamma][\[Mu]_][\[Kappa]_,\[Omega]_] :> -\[CurlyEpsilon][\[Alpha],\[Kappa]]\[CurlyEpsilon][\[Beta],\[Omega]] - \[CurlyEpsilon][\[Alpha],\[Omega]]\[CurlyEpsilon][\[Beta],\[Kappa]],
            \[Gamma][\[Mu]_][OrderlessPatternSequence[\[Alpha]_,\[Beta]_]]\[CurlyEpsilon][x:OrderlessPatternSequence[\[Beta]_,\[Kappa]_]]\[Gamma][\[Nu]_][OrderlessPatternSequence[\[Kappa]_,\[Zeta]_]] :> (If[{x}[[1]]===\[Beta],1,-1] (\[ScriptG][\[Mu],\[Nu]]\[CurlyEpsilon][\[Alpha],\[Zeta]] - \[Epsilon]3[\[Mu],\[Nu],#1]\[Gamma][#1][\[Alpha],\[Zeta]])&@Unique[\[Rho]]),
            \[Gamma][\[Mu]_][\[Alpha]_,\[Beta]_]\[CurlyEpsilon][OrderlessPatternSequence[\[Alpha]_,\[Beta]_]] :> 0
            (*Do we need other rules?*)
};
(*\[Epsilon]3*)       
PauliRules\[Epsilon]3:={\[Epsilon]3[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_]]\[Epsilon]3[\[Mu]_,\[Nu]_,\[Rho]_] :> -6*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho]}]]],
            \[Epsilon]3[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_]]\[Epsilon]3[y:OrderlessPatternSequence[\[Kappa]_,\[Nu]_,\[Rho]_]] :> -2*\[ScriptG][\[Mu],\[Kappa]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Nu],\[Rho]}]]],
            \[Epsilon]3[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_]]\[Epsilon]3[y:OrderlessPatternSequence[\[Kappa]_,\[Tau]_,\[Rho]_]] :> -Det[Table[\[ScriptG][ii,jj],{ii,{\[Mu],\[Nu]}},{jj,{\[Kappa],\[Tau]}}]]*Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho]}]]]*Signature[PermutationList[FindPermutation[{y},{\[Kappa],\[Tau],\[Rho]}]]],
            \[Epsilon]3[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_]]\[Epsilon]3[y:OrderlessPatternSequence[\[Kappa]_,\[Tau]_,\[Omega]_]] :> -Det[Table[\[ScriptG][ii,jj],{ii,{x}},{jj,{y}}]],
            \[Epsilon]3[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_]]\[Gamma][\[Mu]_][\[Alpha]_,\[Beta]_]\[Gamma][\[Nu]_][\[Kappa]_,\[Omega]_] :> - 1/2 Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho]}]]](\[Gamma][\[Rho]][\[Alpha],\[Kappa]]\[CurlyEpsilon][\[Beta],\[Omega]] + \[Gamma][\[Rho]][\[Alpha],\[Omega]]\[CurlyEpsilon][\[Beta],\[Kappa]] + \[Gamma][\[Rho]][\[Beta],\[Omega]]\[CurlyEpsilon][\[Alpha],\[Kappa]] + \[Gamma][\[Rho]][\[Beta],\[Kappa]]\[CurlyEpsilon][\[Alpha],\[Omega]])
};


(*Rules to contract objects with \[CurlyEpsilon] or \[ScriptG]*)
(*The function hold applied on d makes it disregard any signs due to anticommutations of \[Theta] or \[Theta]b*)
ContrRule = {v[x_][\[Mu]_]^2 :> sq[x],
			 v[x_][\[Mu]_]v[y_][\[Mu]_] :> glue[x,y],
			 up[h_?spinorQ][\[Alpha]_]low[k_?spinorQ][\[Alpha]_] :> hold[d][h,k],	 
			 up[X_?spinorQ][\[Alpha]_]\[Gamma][\[Mu]_][\[Alpha]_,\[Beta]_]up[Y_?spinorQ][\[Beta]_]v[V_][\[Mu]_]:>hold[d][X,V,Y],
			 v[W_][\[Mu]_]^2:>glue[W,"sq"],
			 \[Epsilon]3[\[Mu]_,\[Nu]_,\[Rho]_]v[X_][\[Mu]_]v[Y_][\[Nu]_]v[Z_][\[Rho]_]:>d[\[Epsilon]\[Mu]\[Nu]\[Rho],glue[X,"v[\[Mu]]"],glue[Y,"v[\[Nu]]"],glue[Z,"v[\[Rho]]"]],
			 \[Epsilon]3[x:OrderlessPatternSequence[\[Mu]_,\[Nu]_,\[Rho]_]]v[X_][\[Mu]_]v[Y_][\[Nu]_]up[H_][\[Alpha]_]up[Hb_][\[Alpha]d_]\[Gamma][\[Lambda]_][\[Alpha]_,\[Alpha]d_]:>Signature[PermutationList[FindPermutation[{x},{\[Mu],\[Nu],\[Rho]}]]]hold[d][\[Epsilon]\[Mu]\[Nu]\[Rho],glue[X,"v[\[Mu]]"],glue[Y,"v[\[Nu]]"],H,\[Gamma][Symbol["\[Rho]"]],Hb]
};

(*Rules for contracting equal \[Theta]s*)
(*Same\[Theta]ruleOLD = {(up[h_?\[Theta]Q][\[Alpha]_]*a___)**(up[h_?\[Theta]Q][\[Beta]_]*b___) :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]Q][\[Alpha]_]*a___)**(low[h_?\[Theta]Q][\[Beta]_]*b___) :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (up[h_?\[Theta]bQ][\[Alpha]_]*a___)**(up[h_?\[Theta]bQ][\[Beta]_]*b___) :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]bQ][\[Alpha]_]*a___)**(low[h_?\[Theta]bQ][\[Beta]_]*b___) :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b,
             (up[h_?\[Theta]Q][\[Alpha]_]*a___)**(low[h_?\[Theta]Q][\[Beta]_]*b___) :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b,
             (up[h_?\[Theta]bQ][\[Alpha]_]*a___)**(low[h_?\[Theta]bQ][\[Beta]_]*b___) :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]Q][\[Alpha]_]*a___)**(up[h_?\[Theta]Q][\[Beta]_]*b___) :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b,
             (low[h_?\[Theta]bQ][\[Alpha]_]*a___)**(up[h_?\[Theta]bQ][\[Beta]_]*b___) :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b};*)
Same\[Theta]rule = {a___**up[h_?\[Theta]Q][\[Alpha]_]**b___**up[h_?\[Theta]Q][\[Beta]_]**c___ :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]Q][\[Alpha]_]**b___**low[h_?\[Theta]Q][\[Beta]_]**c___ :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**up[h_?\[Theta]bQ][\[Alpha]_]**b___**up[h_?\[Theta]bQ][\[Beta]_]**c___ :> -1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]bQ][\[Alpha]_]**b___**low[h_?\[Theta]bQ][\[Beta]_]**c___ :> 1/2 \[CurlyEpsilon][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**up[h_?\[Theta]Q][\[Alpha]_]**b___**low[h_?\[Theta]Q][\[Beta]_]**c___ :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**up[h_?\[Theta]bQ][\[Alpha]_]**b___**low[h_?\[Theta]bQ][\[Beta]_]**c___ :> 1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]Q][\[Alpha]_]**b___**up[h_?\[Theta]Q][\[Beta]_]**c___ :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b],
             a___**low[h_?\[Theta]bQ][\[Alpha]_]**b___**up[h_?\[Theta]bQ][\[Beta]_]**c___ :> -1/2 \[Delta][\[Alpha],\[Beta]] sq[h] a**b**c (-1)^FN[b]};


(*Apply a replacement rule repeatedly expanding the expression every time*)
MAXITERATIONS=100;
Rex[expr_,rule_]:=FixedPoint[Expand[#]/.rule&,expr,MAXITERATIONS];
Rex[rule_]:=Rex[#,rule]&;


(*Writes any d-expression containing \[Chi] ,s and/or \[Theta](b) by putting explicitly the indices.*)
PutIndices[X__Plus]:=Plus@@(PutIndices/@(List@@X));
PutIndices[X__Times]:=NonCommutativeMultiply@@(PutIndices/@(List@@X));
PutIndices[X__NonCommutativeMultiply]:=NonCommutativeMultiply@@(PutIndices/@(List@@X));
PutIndices[X_^p_]/;IntegerQ[p]&&p>0:=Module[{tempNCM},Flatten[tempNCM@@(Table[PutIndices[X],{\[ScriptT],1,p}])/.{Times->tempNCM,NonCommutativeMultiply->tempNCM},Infinity,tempNCM]/.tempNCM->NonCommutativeMultiply];
PutIndices[X_^p_]:=X^p;
PutIndices[CC_?ConstQ]:=CC;


PutIndices[x_?xsqQ]:=x;
PutIndices[xdx_?xxQ]:=xdx;(*StringCases[toString[xdx],(a:{"x"~~DigitCharacter..})~~(b:{"x"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];*)
PutIndices[d[something__]]/;Select[{something},StringMatchQ[toString[#],("\[Chi]"|"s"|"\[Theta]")~~__]&]==={}:=d[something];
PutIndices[d[a_?spinorQ,b_?spinorQ]]:=(up[a][#]**low[b][#])&@Unique[\[Alpha]];
PutIndices[d[a_?spinorQ,x_?xQ|x_?sQ,b_?spinorQ]]:=(v[x][#3]\[Gamma][#3][#1,#2]up[a][#1]**up[b][#2])&@@{Unique[\[Alpha]],Unique[\[Beta]],Unique[\[Mu]]};
PutIndices[th_?\[Theta]sqQ]:=up[ToExpression[StringDrop[toString[th],-2]]][#]**low[ToExpression[StringDrop[toString[th],-2]]][#]&@Unique[\[Alpha]];
PutIndices[th_?\[Theta]bsqQ]:=up[ToExpression[StringDrop[toString[th],-2]]][#]**low[ToExpression[StringDrop[toString[th],-2]]][#]&@Unique[\[Alpha]];
PutIndices[xdx_?xsQ]:=StringCases[toString[xdx],(a:{"x"~~DigitCharacter..})~~(b:{"s"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];
PutIndices[dxdx_?ssQ]:=StringCases[toString[dxdx],(a:{"s"~~DigitCharacter..})~~(b:{"s"~~DigitCharacter..}):>v[ToExpression[a]][#] v[ToExpression[b]][#]][[1]]&@Unique[\[Mu]];
PutIndices[d[\[Epsilon]\[Mu]\[Nu]\[Rho],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),(z_?xvQ|z_?svQ)]]:=(\[Epsilon]3[#1,#2,#3]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(v[ToExpression[StringDrop[toString[Head[z]],-1]]]@@z)/.{\[Mu]->#1,\[Nu]->#2,\[Rho]->#3}))&@@{Unique[\[Mu]],Unique[\[Nu]],Unique[\[Rho]]};
PutIndices[d[\[Epsilon]\[Mu]\[Nu]\[Rho],(x_?xvQ|x_?svQ),(y_?xvQ|y_?svQ),a_?spinorQ,si_?\[Gamma]Q,b_?spinorQ]]:=(\[Epsilon]3[#1,#2,#3]((v[ToExpression[StringDrop[toString[Head[x]],-1]]]@@x)(v[ToExpression[StringDrop[toString[Head[y]],-1]]]@@y)(si[#4,#5])/.{\[Mu]->#1,\[Nu]->#2,\[Rho]->#3})up[a][#4]**up[b][#5])&@@{Unique[\[Mu]],Unique[\[Nu]],Unique[\[Rho]],Unique[\[Alpha]],Unique[\[Beta]]};


(* ::Subsection::Closed:: *)
(*Removal of placeholders for derivatives \[Chi] and s*)


(*Takes the derivative of the auxiliary variables \[Chi] and s*)
D\[Chi]up[\[Chi]_,\[ScriptA]_] := {low[\[Chi]][\[Beta]_]:>\[CurlyEpsilon][\[Beta],\[ScriptA]], up[\[Chi]][\[Beta]_]:>\[Delta][\[ScriptA],\[Beta]]};
D\[Chi]low[\[Chi]_,\[ScriptA]_] := {up[\[Chi]][\[Beta]_]:>\[CurlyEpsilon][\[Beta],\[ScriptA]], low[\[Chi]][\[Beta]_]:>\[Delta][\[ScriptA],\[Beta]]};
\[Gamma]Dx[s_,\[ScriptA]_,\[ScriptA]d_] := {v[s][\[Mu]_]:>\[Gamma][\[Mu]][\[ScriptA],\[ScriptA]d]};
Dx\[Mu][s_,\[ScriptM]_] := {v[s][\[Nu]_]:>\[ScriptG][\[ScriptM],\[Nu]]};


\[Theta]OrderedList[mon_Times|mon_NonCommutativeMultiply|mon_d]:=Module[{tempNCM},
    (*FN[x_tempNCM]:=Mod[Plus@@(FN/@List@@x),2]; This might be needed?*)
    Join[Flatten[tempNCM[mon]/.b_^p_/;IntegerQ[p]&&p>0:>tempNCM@@Table[b,{\[ScriptT],1,p}]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM},Infinity,tempNCM]/.{low[a_][b_]:>a,up[a_][b_]:>a}/.{tempNCM[seq__] :> Cases[{seq},_?\[Theta]\[Theta]bQ]},
         Cases[mon,t_?\[Theta]\[Theta]bsqQ:>Sequence[ToExpression[StringDrop[toString[t],-2]],ToExpression[StringDrop[toString[t],-2]]],Infinity]]
];
\[Theta]OrderedList[b_^p_?IntegerQ]/;p>0:=Flatten[Table[\[Theta]OrderedList[b],{\[ScriptT],1,p}]];
\[Theta]OrderedList[b_^p_]:=\[Theta]OrderedList[b] (*This is ok as long as \[Theta]s don't appear inside real powers. They have to be expanded first*)
\[Theta]OrderedList[mon_]/;(Head[Head[mon]]===up||Head[Head[mon]]===low):=Cases[{mon[[0,1]]},t_?\[Theta]\[Theta]bQ];
\[Theta]OrderedList[mon_]/;(Head[Head[mon]]===v):={};
\[Theta]OrderedList[mon_?\[Theta]\[Theta]bsqQ]:={ToExpression[StringDrop[toString[mon],-2]],ToExpression[StringDrop[toString[mon],-2]]};
\[Theta]OrderedList[mon_?xxQ|mon_?xsqQ]:={};
\[Theta]OrderedList[sym_?ConstQ]:={};
\[Theta]OrderedList[sym_]/;Head[sym]===\[Delta]:={};
\[Theta]OrderedList[sym_]/;Head[sym]===\[CurlyEpsilon]:={};


(*This function takes an expression with \[Chi]'s and s's that need to be contracted*)
(*The parameters are:
  derivative : the replacement used to contract \[Chi] and s (it's a pure function, not a replacement table)
  redRules : (Optional) specify the reduction rules
  contRules : (Optional) specify the reduction rules
*)
(*Note: the order PauliRed,PauliRulesgen matters: if you do PauliRulesgen before it'll contract stuff in a stupid way and require more identities*)
Rulesremove\[Chi]s=<|redRules:>Join[PauliRulesgen,PauliRed,PauliRules\[Epsilon]3],contRules:>Join[PauliRulesgen,ContrRule]|>;
remove\[Chi]s::contractionFail="I couldn't contract the indices in the following expression:\n `1`";
(*This prevents Auxremeve\[Chi]s to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[Auxremove\[Chi]s,HoldRest];
remove\[Chi]s[expr_,derivative_]:=Module[{expanded},Plus@@(Auxremove\[Chi]s[#,derivative,Rulesremove\[Chi]s[redRules],Rulesremove\[Chi]s[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
Auxremove\[Chi]s[a_?ConstQ*b_,derivative_,red_,cont_]:=a Auxremove\[Chi]s[b,derivative,red,cont];
Auxremove\[Chi]s[a_?ConstQ,derivative_,red_,cont_]:=a;
Auxremove\[Chi]s[expr_Times|expr_d|expr_NonCommutativeMultiply|expr_Symbol,derivative_,red_,cont_]:=Module[{uptosign,uptosignl,iniorder},
   (*applies the derivative and the rule for same \[Theta]s*)
     uptosign=(derivative[PutIndices[expr]]//.Same\[Theta]rule);
   (*memorizes the initial order of \[Theta]s*)
     iniorder = \[Theta]OrderedList[uptosign];
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign/.NonCommutativeMultiply->Times//Rex[red]//Rex[cont])/.Times-> NonCommutativeMultiply;
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Puts to zero terms with too many \[Theta] or \[Theta]b*)
     uptosignl = DeleteCases[(If[tooMany\[Theta]Q[#]||tooMany\[Theta]bQ[#],Nothing,#]&)/@uptosignl,0];(*Print[iniorder,">>>",uptosignl];*)
   (*Checking for this error message could waste some time. For now let's have it commented out*)
     (*Do[If[Cases[el,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity]=!={},Message[remove\[Chi]s::contractionFail,Cases[el,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]4[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Sigma][m__][n__],Infinity]]],{el,uptosignl}];*)
   (*Returns the sum where terms not agreeing with the initial ordering are multiplied by the signature of the permutation*)
     Plus@@(Signature[PermutationList[Check[FindPermutation[\[Theta]OrderedList[#],iniorder],Message[remove\[Chi]s::contractionFail,Times@@(Cases[#,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]3[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Gamma][m_][n__],Infinity])];$Failed,FindPermutation::norel]]]*#&/@uptosignl)
];


(* ::Subsection::Closed:: *)
(*Reduction*)


RulesReduction=<|redRules:>Join[PauliRulesgen,PauliRed,PauliRules\[Epsilon]3],contRules:>Join[PauliRulesgen,ContrRule]|>;
Reduction::contractionFail="I couldn't contract the indices in the following expression:\n `1`";
(*This prevents AuxReduction to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[AuxReduction,HoldRest];
Reduction[expr_]:=Module[{expanded},Plus@@(AuxReduction[#,RulesReduction[redRules],RulesReduction[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
AuxReduction[a_?ConstQ*b_,red_,cont_]:=a AuxReduction[b,red,cont];
AuxReduction[a_?ConstQ,red_,cont_]:=a;
AuxReduction[expr_,red_,cont_]:=Module[{exprNCM,tempNCM,uptosign,uptosignl,iniorder},
   (*applies the rule for same \[Theta]s*)
     uptosign=(Flatten[tempNCM[PutIndices[expr]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.tempNCM->NonCommutativeMultiply)//Rex[Same\[Theta]rule];    (*Here you need to make sure that you apply all possible Same\[Theta]rule, because afterwards you take away the ** and you may miss some signs*)
   (*memorizes the initial order of \[Theta]s*)
     iniorder = \[Theta]OrderedList[uptosign];
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign/.NonCommutativeMultiply->Times//Rex[red]//Rex[cont])/.Times-> NonCommutativeMultiply;
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Puts to zero terms with too many \[Theta] or \[Theta]b*)
     uptosignl = DeleteCases[(If[tooMany\[Theta]Q[#]||tooMany\[Theta]bQ[#],Nothing,#]&)/@uptosignl,0];
   (*Returns the sum where terms not agreeing with the initial ordering are multiplied by the signature of the permutation*)
     Plus@@(Signature[PermutationList[Check[FindPermutation[\[Theta]OrderedList[#],iniorder],Message[Reduction::contractionFail,Times@@(Cases[#,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]3[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Gamma][m_][n__],Infinity])];$Failed,FindPermutation::norel]]]*#&/@uptosignl)
]


(* ::Subsection::Closed:: *)
(*Memoize and apply functions*)


memoizeAndEvaluate[f_][expr_]:=Module[{set},
    set[Activate[Inactivate[f[expr]],Function],
        f[expr]
        ]/.set[a__]:>Hold[set[a]]/.Inactive[x_]:>x/.set->Set//ReleaseHold]


fastApply[f_][expr_]:=Module[{all,\[Theta]Lis,xsqLis,\[ScriptI]=1,ruledelayed,expression},
    If[working\[Theta]s === {} || workingxs === {},
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all//.{
          x_?\[Gamma]Q->Nothing,x_Power->Nothing,
          x_?\[Theta]\[Theta]bsqQ :> ToExpression[StringDrop[toString[x],-2]]}];
    \[Theta]Lis = Join[Select[all,\[Theta]Q],Select[all,\[Theta]bQ]];
    xsqLis = Select[all,xsqQ[#]||xxQ[#]&];,
    \[Theta]Lis = working\[Theta]s;
    xsqLis = workingxs;];
    THETA/:ConstQ[THETA[i_]]:=True;
    expression=(Collect[Rex[
        Join[
            Table[ruledelayed[th \[ScriptD][yPattern___], \[ScriptD][yPattern,th]]/.ruledelayed->RuleDelayed,{th,sq/@\[Theta]Lis}],
            Table[ruledelayed[xsqr^pPattern_. \[ScriptD][yPattern___], \[ScriptD][yPattern,xsqr^pPattern]]/.ruledelayed->RuleDelayed,{xsqr,xsqLis}],
           {HoldPattern[NonCommutativeMultiply[x__]\[ScriptD][y___]]:>\[ScriptD][y,NonCommutativeMultiply[x]],
           d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j], A_^pow_ \[ScriptD][y___] :> \[ScriptD][y,A^pow]}
        ]][
       Expand[(expr/.Flatten[Table[{th -> THETA[\[ScriptI]] th, sq[th] -> THETA[\[ScriptI]++]^2 sq[th]},{th,\[Theta]Lis}]]/.{
               THETA[i_]^p_/;p>2 -> 0}/.{THETA[i_]->1}) \[ScriptD][]]],
    \[ScriptD][___],THETA[Factor[#]]&]/.\[ScriptD][x___]:>If[Head[f]===Function,memoizeAndEvaluate[f][Times[x]],(f[Times[x]]=f[Times[x]])]);
    Collect[expression, THETA[___]]/.THETA[x_]:>x
];


fastReduction[expr_]:=Module[{all,\[Theta]Lis,\[ScriptI]=1,ruledelayed,expression},
    If[working\[Theta]s === {},
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all//.{
          x_?\[Gamma]Q->Nothing,x_Power->Nothing,
          x_?sqQ :> ToExpression[StringDrop[toString[x],-2]]}];
    \[Theta]Lis = Join[Select[all,\[Theta]Q],Select[all,\[Theta]bQ]],
    \[Theta]Lis = working\[Theta]s];
    THETA/:ConstQ[THETA[i_]]:=True;
    expression=(Collect[Rex[
        Join[
            Table[ruledelayed[th \[ScriptD][yPattern___], \[ScriptD][yPattern,th]]/.ruledelayed->RuleDelayed,{th,sq/@\[Theta]Lis}],
           {HoldPattern[NonCommutativeMultiply[x__]\[ScriptD][y___]]:>\[ScriptD][y,NonCommutativeMultiply[x]],d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j]}
        ]][
       (Expand[expr/.Flatten[Table[{th -> THETA[\[ScriptI]] th, sq[th] -> THETA[\[ScriptI]++]^2 sq[th]},{th,\[Theta]Lis}]]
               ]/.{THETA[i_]^p_/;p>2 -> 0}/.{THETA[i_]->1}) \[ScriptD][]],
    \[ScriptD][___],THETA[Factor[#]]&]/.\[ScriptD]->Times);
    Collect[expression, THETA[___], (Reduction[#]=Reduction[#])&]/.THETA[x_]:>x
];


(* ::Subsection::Closed:: *)
(*From an input with explicit indices*)


RulesContraction=<|redRules:>Join[PauliRulesgen,PauliRed,PauliRules\[Epsilon]3],contRules:>Join[PauliRulesgen,ContrRule]|>;
(*This prevents Contraction to evaluate the rules red and cont, so that they evaluate everytime they are called*)
SetAttributes[AuxContraction,HoldRest];
Contraction[expr_]:=Module[{expanded},Plus@@(AuxContraction[#,RulesContraction[redRules],RulesContraction[contRules]]&/@(If[(Head[expanded=Expand[expr]])===Plus,List@@expanded,{expanded}]))];
AuxContraction[a_?NumericQ*b_,red_,cont_]:=a AuxContraction[b,red,cont];
AuxContraction[a_?NumericQ,red_,cont_]:=a;
AuxContraction[expr_,red_,cont_]:=Module[{exprNCM,tempNCM,uptosign,uptosignl,iniorder},
   (*applies the derivative and the rule for same \[Theta]s*)
     uptosign=(Flatten[tempNCM[expr/.d[X__]^p_:>PutIndices[d[X]^p]/.{d[A__]:>PutIndices[d[A]],X_?\[Theta]\[Theta]bsqQ:>PutIndices[X]
                                     }/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.tempNCM->NonCommutativeMultiply)//Rex[Same\[Theta]rule];    (*Here you need to make sure that you apply all possible Same\[Theta]rule, because afterwards you take away the ** and you may miss some signs*)
   (*memorizes the initial order of \[Theta]s*)
     iniorder = \[Theta]OrderedList[uptosign];
   (*removes the ** product and applies all reduction rules*)
     uptosign=(uptosign/.NonCommutativeMultiply->Times//Rex[red]//Rex[cont])/.Times-> NonCommutativeMultiply;
   (*lists the terms*)
     uptosignl=If[Head[uptosign]===Plus,List@@(Expand[uptosign]),{uptosign}];
   (*Puts to zero terms with too many \[Theta] or \[Theta]b*)
     uptosignl = DeleteCases[(If[tooMany\[Theta]Q[#]||tooMany\[Theta]bQ[#],Nothing,#]&)/@uptosignl,0];
   (*Returns the sum where terms not agreeing with the initial ordering are multiplied by the signature of the permutation*)
     Plus@@(Signature[PermutationList[Check[FindPermutation[\[Theta]OrderedList[#],iniorder],Message[Reduction::contractionFail,Times@@(Cases[#,up[x_][y_]|low[z_][t_]|v[o_][p_]|\[Epsilon]3[w__]|\[Delta][v__]|\[CurlyEpsilon][u__]|\[Gamma][m_][n__],Infinity])];$Failed,FindPermutation::norel]]]*#&/@uptosignl)
]


(* ::Subsection::Closed:: *)
(*Taylor expansion*)


(*Raises an expression to a positive integer power*)
FPower[expr_,p_?IntegerQ]/;p>0:=Reduction[NonCommutativeMultiply@@Table[expr,{i,1,p}]];
FPower[expr_,0]:=1;
(*Raises an expression to a real power*)
FPower[expr_,around_,p_,n_]/;\[Theta]FreeQ[around]:=around^p Sum[Binomial[p,k]FPower[(expr-around)/around,k],{k,0,n}];
(*Computes the Taylor expansion of f[expr] around expr = 0.*)
FTaylor[expr_,f_,n_]:=Sum[ SeriesCoefficient[f[x],{x,0,k}] FPower[expr,k],{k,0,n}]


(* ::Section::Closed:: *)
(*Derivatives*)


(* ::Subsection::Closed:: *)
(*Definitions*)


(*Leibniz and graded Leibniz rules. These are supposed to be used only on expressions with head Times or NonCommutativeMultiply*)
Leibniz[f_]:=Module[{factors=List@@#},
    Plus@@Table[(MapAt[Function[{x},f[x,##2]],factors,iter]/.List->Head[#]),{iter,1,Length@factors}]
]&;
(*GradedLeibnizOld[f_]:=Module[{factors,tempNCM,grading},
    factors = Flatten[tempNCM[#]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM},Infinity,tempNCM];
    factors = If[Head[factors]===tempNCM,List@@factors,{factors}]/.tempNCM->Times;
    grading[list_]:=(-1)^(Plus@@(FN/@(list/.tempNCM->Times)));
    Plus@@Table[grading[factors[[1;;iter-1]]]*(MapAt[Function[{x},f[x,##2]],factors,iter]/.List-> NonCommutativeMultiply/.tempNCM->NonCommutativeMultiply),{iter,1,Length@factors}]   
]&;*)
GradedLeibniz[f_]:=Module[{expr = #, expanded, sumexpr, factors, tempNCM, grading},
    expanded = Expand[expr];
    sumexpr = If[Head[expanded]===Plus,List@@expanded, {expanded}];
    grading[list_]:=(-1)^(Plus@@(FN/@(list/.tempNCM->Times)));
    Sum[
        factors = Flatten[tempNCM[ex]/.{Times->tempNCM,NonCommutativeMultiply->tempNCM},Infinity,tempNCM];
        factors = If[Head[factors]===tempNCM,List@@factors,{factors}]/.tempNCM->Times;
        Plus@@Table[grading[factors[[1;;iter-1]]]*(MapAt[Function[{x},f[x,##2]],factors,iter]/.List-> NonCommutativeMultiply/.tempNCM->NonCommutativeMultiply),{iter,1,Length@factors}]
    ,{ex, sumexpr}]
]&;
dPower[f_,\[ScriptL]_]:=\[ScriptL] #^(\[ScriptL]-1) f[#,##2]&;


applyRuleSequence[rules__][expr_]:=Fold[#1/.#2&,expr,{rules}];


\[Eta]to\[Chi][sym_Symbol]:=ToExpression[StringReplace[ToString[sym],{"\[Eta]"->"\[Chi]","x"->"s"}]];


(* ::Subsection::Closed:: *)
(*\[Chi]\[PartialD]\[Eta], \[Chi]\[PartialD]\[Theta] and s\[PartialD]x*)


(*These functions' only purpose is to replace \[Eta],\[Theta] with \[Chi] and x with s and apply the Leibniz rule in doing so*)


(*\[Chi]\[PartialD]\[Eta] = \[Chi]^\[Alpha]\[PartialD]/\[PartialD]\[Eta]^\[Alpha] or = Overscript[\[Chi], _]^Overscript[\[Alpha], .]\[PartialD]/\[PartialD]Overscript[\[Eta], _]^Overscript[\[Alpha], .]*)
(*Define the fermion numbers of this operator*)
\[Chi]d\[Eta]/:FN[\[Chi]d\[Eta][expr_,\[Eta]_,\[Chi]_]]:=FN[expr];
(*Define the behaviour on +, * and ** *)
\[Chi]d\[Eta][expr_Plus,\[Eta]_,\[Chi]_]:=Plus@@(\[Chi]d\[Eta][#,\[Eta],\[Chi]]&/@List@@(expr));
\[Chi]d\[Eta][expr_Times|expr_NonCommutativeMultiply,\[Eta]_,\[Chi]_]:=Leibniz[\[Chi]d\[Eta]][expr,\[Eta],\[Chi]];
\[Chi]d\[Eta][expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[\[Chi]d\[Eta],\[ScriptL]][expr,\[Eta],\[Chi]];
\[Chi]d\[Eta][expr_?ConstQ,\[Eta]_,\[Chi]_]:=0;
(*Actual definition*)
\[Chi]d\[Eta][expr_d,\[Eta]_,\[Chi]_]/;Length[Cases[expr,\[Eta]]]==1:=expr/.\[Eta]->\[Chi];
\[Chi]d\[Eta][expr_d,\[Eta]_,\[Chi]_]/;FreeQ[expr,\[Eta]]:=0;
\[Chi]d\[Eta][d[\[Eta]_,x_?xQ|x_?sQ,\[Eta]_],\[Eta]_,\[Chi]_]:=2d[\[Chi],x,\[Eta]];
\[Chi]d\[Eta][d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1_,x2_,\[Eta]_,\[Gamma][\[Rho]_],\[Eta]_],\[Eta]_,\[Chi]_]:=2d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1,x2,\[Chi],\[Gamma][\[Rho]],\[Eta]];
\[Chi]d\[Eta][a_?xxQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?xsQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?ssQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Eta][a_?sqQ,\[Eta]_,\[Chi]_]:=0;


(*\[Chi]\[PartialD]\[Theta] = \[Chi]^\[Alpha]\[PartialD]/\[PartialD]\[Theta]^\[Alpha]*)
(*Define the fermion numbers of this operator*)
\[Chi]d\[Theta]/:FN[\[Chi]d\[Theta][expr_,\[Theta]_,\[Chi]_]]:=Mod[FN[expr]+1,2];
(*Define the behaviour on +, * and ** *)
\[Chi]d\[Theta][expr_Plus,\[Theta]_,\[Chi]_]:=Plus@@(\[Chi]d\[Theta][#,\[Theta],\[Chi]]&/@List@@(expr));
\[Chi]d\[Theta][expr_Times|expr_NonCommutativeMultiply,\[Theta]_,\[Chi]_]:=GradedLeibniz[\[Chi]d\[Theta]][expr,\[Theta],\[Chi]];
\[Chi]d\[Theta][expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[\[Chi]d\[Theta],\[ScriptL]][expr,\[Eta],\[Chi]];
\[Chi]d\[Theta][expr_?ConstQ,\[Theta]_,\[Chi]_]:=0;
(*Actual definition*)
\[Chi]d\[Theta][d[Th_,Th2_],Th_,\[Chi]_]:=d[\[Chi],Th2];
\[Chi]d\[Theta][d[Th2_,Th_],Th_,\[Chi]_]:=((-1)^FN[Th2])d[Th2,\[Chi]];
\[Chi]d\[Theta][d[Th_,x_,Th2_],Th_,\[Chi]_]:=d[\[Chi],x,Th2];
\[Chi]d\[Theta][d[Th2_,x_,Th_],Th_,\[Chi]_]:=((-1)^FN[Th2])d[Th2,x,\[Chi]];
\[Chi]d\[Theta][d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1_,x2_,\[Theta]_,\[Gamma][\[Rho]_],\[Theta]b_],\[Theta]_,\[Chi]_]:=d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1,x2,\[Chi],\[Gamma][\[Rho]],\[Theta]b];
\[Chi]d\[Theta][d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1_,x2_,\[Theta]_,\[Gamma][\[Rho]_],\[Theta]b_],\[Theta]b_,\[Chi]b_]:=((-1)^FN[\[Theta]])d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1,x2,\[Theta],\[Gamma][\[Rho]],\[Chi]b];
\[Chi]d\[Theta][expr_d,\[Theta]_,\[Chi]_]/;FreeQ[expr,\[Theta]]:=0;
\[Chi]d\[Theta][expr_?\[Theta]\[Theta]bsqQ,\[Theta]_,\[Chi]_]:=If[StringMatchQ[toString[expr],toString[\[Theta]]~~"sq"],2d[\[Chi],\[Theta]],0];
\[Chi]d\[Theta][a_?xxQ,\[Theta]_,\[Chi]_]:=0;
\[Chi]d\[Theta][a_?xsQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Theta][a_?ssQ,\[Eta]_,\[Chi]_]:=0;
\[Chi]d\[Theta][a_?sqQ,\[Theta]_,\[Chi]_]:=0;


(*s\[PartialD]x = s^\[Mu]\[PartialD]/\[PartialD]x^\[Mu]*)
(*Define the fermion numbers of this operator*)
sdx/:FN[sdx[expr_,x_,s_]]:=FN[expr];
(*Define the behaviour on +, * and ** *)
sdx[expr_Plus,x_,s_]:=Plus@@(sdx[#,x,s]&/@List@@(expr));
sdx[expr_Times|expr_NonCommutativeMultiply,x_,s_]:=Leibniz[sdx][expr,x,s];
sdx[expr_^\[ScriptL]_,\[Eta]_,\[Chi]_]:=dPower[sdx,\[ScriptL]][expr,\[Eta],\[Chi]];
sdx[expr_?ConstQ,x_,s_]:=0;
(*These two functions return 1,-1 or 0: e.g xMatchQ[x12,x1]=1,xMatchQ[x12,x2]=-1,xMatchQ[x12,x3]=0*)
xMatchQ[xx_,x_]:=If[StringMatchQ[ToString[xx],ToString[x]~~DigitCharacter..],1,If[StringMatchQ[ToString[xx],"x"~~DigitCharacter~~StringTake[ToString[x],-1]],-1,If[StringMatchQ[ToString[xx],ToString[x]],1,0]]];
xvMatchQ[xx_,x_]:=If[StringMatchQ[ToString[Head[xx]],ToString[x]~~DigitCharacter..~~"v"],1,If[StringMatchQ[ToString[Head[xx]],"x"~~DigitCharacter~~StringTake[ToString[x],-1]~~"v"],-1,If[StringMatchQ[ToString[Head[xx]],ToString[x]~~"v"],1,0]]];
(*Actual definition*)
sdx[d[H_,xx_,Hb_],x_,s_]:=xMatchQ[xx,x]d[H,s,Hb];
sdx[d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1_?xvQ|x1_?svQ,x2_?xvQ|x2_?svQ,H1_,si_?\[Gamma]Q,H2_],x_,s_]:=xvMatchQ[x1,x]d[\[Epsilon]\[Mu]\[Nu]\[Rho],Symbol[ToString[s]<>"v"]@@x1,x2,H1,si,H2]+xvMatchQ[x2,x]d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1,Symbol[ToString[s]<>"v"]@@x2,H1,si,H2];
sdx[d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1_?xvQ|x1_?svQ,x2_?xvQ|x2_?svQ,x3_?xvQ|x3_?svQ],x_,s_]:=xvMatchQ[x1,x]d[\[Epsilon]\[Mu]\[Nu]\[Rho],Symbol[ToString[s]<>"v"]@@x1,x2,x3]+xvMatchQ[x2,x]d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1,Symbol[ToString[s]<>"v"]@@x2,x3]+xvMatchQ[x3,x]d[\[Epsilon]\[Mu]\[Nu]\[Rho],x1,x2,Symbol[ToString[s]<>"v"]@@x3];
sdx[a_?sqQ,x_,s_]:=xMatchQ[StringDrop[ToString[a],-2],x]*2*Symbol[StringDrop[ToString[a],-2]<>ToString[s]];
sdx[a_?xxQ,x_,s_]:=Module[{aa},aa=StringCases[ToString[a],{(e:("x"~~DigitCharacter..))~~(f:("x"~~DigitCharacter..)):>{e,f}}][[1]];xMatchQ[aa[[1]],x]Symbol[aa[[2]]<>ToString[s]]+xMatchQ[aa[[2]],x]Symbol[aa[[1]]<>ToString[s]]];
sdx[a_?xsQ,x_,s_]:=Module[{aa},aa=StringCases[ToString[a],{(e:("x"~~DigitCharacter..))~~(f:("s"~~DigitCharacter..)):>{e,f},(e:("s"~~DigitCharacter..))~~(f:("x"~~DigitCharacter..)):>{f,e}}][[1]];xMatchQ[aa[[1]],x]Symbol[aa[[2]]<>ToString[s]]];
sdx[d[a_,b_],x_,s_]:=0;


(*This is an alias, but careful: the arguments are swapped!*)
\[Eta]d\[Eta][expr_,\[Chi]_,\[Eta]_]:=\[Chi]d\[Eta][expr,\[Eta],\[Chi]];
\[Eta]d\[Theta][expr_,\[Chi]_,\[Theta]_]:=\[Chi]d\[Theta][expr,\[Theta],\[Chi]];
\[Eta]d\[Theta]b[expr_,\[Chi]_,\[Theta]b_]:=\[Chi]d\[Theta][expr,\[Theta]b,\[Chi]];
xdx[expr_,s_,x_]:=sdx[expr,x,s] /. xxx_Symbol /; StringMatchQ[toString[xxx],RegularExpression["(x\\d+)\\1"]] :> ToExpression[StringReplace[toString[xxx],RegularExpression["(x\\d+)\\1"] -> "$1sq"]];


(* ::Subsection::Closed:: *)
(*Composite operators*)


defineD::argumentsNotPresent="The arguments `2` are not all present in `1`.";
SetAttributes[defineD,HoldFirst];


(*This is a function that assigns to symbol a differential operator that is given by the expression operator. 
   The elements in derArguments are the derivatives and the ones in multArguments are the ones that appear multiplied.*)
defineD[symbol_Symbol,operator_,derArguments_List,multArguments_List]:=Module[{
    arguments = Join[derArguments,multArguments],
    usageMessage,
    multArgumentsUnique, multArgumentsUniqueRule,
    derArguments\[Chi]Rule,
    derArgumentsOrdered, derArgumentsBut\[Chi],
    expandedOp,
    replacementRules={},elementaryDerivatives={},
    dsd\[Eta]d\[Theta],opRule,tempNCM
    },
    
    If[StringQ[symbol::usage],
        usageMessage=symbol::usage,
        usageMessage=""
    ];
    
    ClearAll[symbol];
    symbol::usage = usageMessage;
    symbol/:FN[symbol[expr_,args_]]:=Evaluate[FN[operator]];
    
    (*If[Or@@(Length[Cases[operator,#,\[Infinity]]]==0&/@arguments),
        Message[defineD::argumentsNotPresent,operator,arguments]
    ];*)
    
    multArgumentsUniqueRule = Table[marg -> Unique[ToString[marg]],{marg,multArguments}];
    multArgumentsUnique = multArguments /. multArgumentsUniqueRule;
    
    derArguments\[Chi]Rule = Join[
        Table[darg -> \[Eta]to\[Chi][darg],{darg,derArguments}],
        Table[$sequence[
            sq[darg] -> glue[\[Eta]to\[Chi][darg],\[Eta]to\[Chi][darg]],
            With[{sdarg=ToString[darg],ssdarg=ToString[\[Eta]to\[Chi][darg]]},
                sss_Symbol/;xxQ[sss] :> ToExpression[StringReplace[ToString[sss],sdarg -> ssdarg]]]
            ]
        ,{darg,Select[derArguments,xQ]}]/.$sequence -> Sequence
    ];
    derArgumentsBut\[Chi] = derArguments /. derArguments\[Chi]Rule;

    expandedOp = PutIndices[operator/.derArguments\[Chi]Rule] /. multArgumentsUniqueRule;
    
    derArgumentsOrdered = Cases[
        Flatten[
            expandedOp /. {Times->tempNCM,NonCommutativeMultiply->tempNCM},
            \[Infinity],tempNCM]/.{low[x_][a_]:>x,up[x_][a_]:>x,v[x_][m_]:>x,\[Gamma][_][__]->Nothing,nn_?NumericQ->Nothing}/.tempNCM->List,
        Alternatives@@(derArgumentsBut\[Chi])];
            
    expandedOp = expandedOp/.Join@@Table[With[{chiOrs=Unique[StringReplace[ToString[ar],{
                "x"~~DigitCharacter.. -> "s", ("\[Theta]"|"\[Theta]b")~~DigitCharacter.. -> "\[Chi]"}]],
                \[Theta]or\[Eta]Q = If[StringMatchQ[ToString[ar],"\[Chi]"~~DigitCharacter..],\[Chi]d\[Eta],\[Chi]d\[Theta]],
                arReplace = ar},{
            up[ar][alph_] :> (
                AppendTo[replacementRules,D\[Chi]low[chiOrs,alph]];
                AppendTo[elementaryDerivatives,arReplace->(\[Theta]or\[Eta]Q[#,arReplace,chiOrs]&)];
                1),
            low[ar][alph_] :> (
                AppendTo[replacementRules,D\[Chi]up[chiOrs,alph]];
                AppendTo[elementaryDerivatives,arReplace->(\[Theta]or\[Eta]Q[#,arReplace,chiOrs]&)];
                1),
            v[ar][mu_] :> (
                AppendTo[replacementRules,Dx\[Mu][chiOrs,mu]];
                AppendTo[elementaryDerivatives,arReplace->(sdx[#,arReplace,chiOrs]&)];
                1)
        }]
    ,{ar,derArgumentsOrdered}];
    
    opRule = With[{ll=Length[derArguments]+1, multArgumentsUniqueReplaced=multArgumentsUnique, 
        replacementRulesReplaced=replacementRules, expandedOpReplaced=expandedOp /. {Times->tempNCM,NonCommutativeMultiply->tempNCM}},
        Function[{derivedExpr,argsList},
            With[{expanded=Expand[derivedExpr]},
                Plus@@((Flatten[tempNCM[
                        expandedOpReplaced,
                        # /. {Times->tempNCM,NonCommutativeMultiply->tempNCM}
                    ],\[Infinity],tempNCM]//applyRuleSequence[Sequence@@replacementRulesReplaced,tempNCM->NonCommutativeMultiply]
                )& /@ If[Head[expanded]===Plus,List@@expanded,{expanded}]
            )/.Thread[multArgumentsUniqueReplaced -> argsList[[ll;;-1]]]]
        ]
    ];
    
    dsd\[Eta]d\[Theta] = With[{LL=Length[arguments], ll=Length[derArguments],elementaryDerivativesReplaced=elementaryDerivatives,derArgumentsReplaced=derArgumentsOrdered},
        $function[{$initialExpr,Sequence@@Table[glue["arg",nnn],{nnn,1,LL}]},
            ($composition@@(derArgumentsReplaced/.elementaryDerivativesReplaced))[$initialExpr]/.Thread[derArgumentsBut\[Chi] -> Table[glue["arg",nnn],{nnn,1,ll}]]
    ]]/.$function->Function/.$composition->Composition;
    
    symbol[expr_Plus,args__]:=Plus@@(symbol[#,args]&/@List@@(expr));
    symbol[expr_?ConstQ,args__]:=0;
    With[{dsd\[Eta]d\[Theta]Replaced=dsd\[Eta]d\[Theta],opRuleReplaced=opRule},
        symbol[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,args__]:=remove\[Chi]s[dsd\[Eta]d\[Theta]Replaced[expr,args],opRuleReplaced[#,{args}]&]
    ];
];


(* ::Subsection::Closed:: *)
(*Predefined operators*)


(*Begin["SpinorAlgebra3d`"];*)


operators$to$define = {
    d\[Eta]dx\[Theta] -> {d[\[Eta]111,x111,\[Theta]111],{\[Eta]111,x111},{\[Theta]111}},
    \[Eta]dx\[Theta]wait -> {d[\[Eta]111,x111,\[Theta]111],{x111},{\[Eta]111,\[Theta]111}},
    d\[Theta]dx\[Eta] -> {d[\[Theta]111,x111,\[Eta]111],{\[Theta]111,x111},{\[Eta]111}},
    \[Theta]d\[Eta]wait   -> {d[\[Eta]111,\[Theta]111],{\[Eta]111},{\[Theta]111}},
    d\[Theta]d\[Eta]  -> {d[\[Theta]111,\[Eta]111],{\[Theta]111,\[Eta]111},{}},
    d\[Eta]d\[Eta]  -> {d[\[Eta]111,\[Eta]112],{\[Eta]111,\[Eta]112},{}},
    \[Eta]dx\[Eta]wait  -> {d[\[Eta]111,x111,\[Eta]112],{x111},{\[Eta]111,\[Eta]112}},
    d\[Eta]dxd\[Eta]-> {d[\[Eta]111,x111,\[Eta]112],{\[Eta]111,x111,\[Eta]112},{}},
    d\[Theta]dxd\[Eta]-> {d[\[Theta]111,x111,\[Eta]112],{\[Theta]111,x111,\[Eta]112},{}},
    d\[Theta]dxd\[Theta]-> {d[\[Theta]111,x111,\[Theta]112],{\[Theta]111,x111,\[Theta]112},{}},
    d\[Eta]xd\[Eta]wait -> {d[\[Eta]111,x111,\[Eta]112], {\[Eta]111,\[Eta]112},{x111}},
    d\[Theta]dx\[Theta]-> {d[\[Theta]111,x111,\[Theta]112],{\[Theta]111,x111},{\[Theta]112}},
    d\[Eta]dx\[Eta]-> {d[\[Eta]111,x111,\[Eta]112],{\[Eta]111,x111},{\[Eta]112}}
};


Do[ 
    With[{sym = op[[1]], fsym = glue["f", op[[1]]]},
        defineD[sym,op[[2,1]],op[[2,2]],op[[2,3]]];
        fsym[expr_, vars__] := fastApply[sym[#,vars]&][expr]],
{op, operators$to$define}];


\[Eta]dx\[Theta][expr_,et_,ex_,th_] := \[Eta]dx\[Theta]wait[expr,ex,et,th];
\[Theta]d\[Eta][expr_,th_,et_] := \[Theta]d\[Eta]wait[expr,et,th];
\[Eta]dx\[Eta][expr_,et1_,ex_,et2_] := \[Eta]dx\[Eta]wait[expr,ex,et1,et2];
d\[Eta]xd\[Eta][expr_,et1_,ex_,et2_] := d\[Eta]xd\[Eta]wait[expr,et1,et2,ex];
(**)
f\[Eta]dx\[Theta][expr_,et_,ex_,th_] := fastApply[\[Eta]dx\[Theta]wait[#,ex,et,th]&][expr];
f\[Theta]d\[Eta][expr_,th_,et_] := fastApply[\[Theta]d\[Eta]wait[#,et,th]&][expr];
f\[Eta]dx\[Eta][expr_,et1_,ex_,et2_] := fastApply[\[Eta]dx\[Eta]wait[#,ex,et1,et2]&][expr];
fd\[Eta]xd\[Eta][expr_,et1_,ex_,et2_] := fastApply[d\[Eta]xd\[Eta]wait[#,et1,et2,ex]&][expr];


(*End[];*)


(* ::Subsection::Closed:: *)
(*\[PartialD]xsq*)


(*\[PartialD]xsq*)
(*Define the fermion numbers of this operator*)
dxsq/:FN[dxsq[expr_,x_]]:=FN[expr];

(*Define the differentiation rules*)
dxsqRule[expr_,s1_,s2_]:=(expr*\[ScriptG][#1,#2]/.v[s1][m_]:>\[ScriptG][m,#1]/.v[s2][n_]:>\[ScriptG][n,#2])&@@{Unique[\[Mu]],Unique[\[Nu]]};

(*Definition*)
dxsq[expr_Plus,x_]:=Plus@@(dxsq[#,x]&/@List@@(expr));
dxsq[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Symbol|expr_Power,x_]:=Block[{s1,s2},s1=Unique["s"];s2=Unique["s"];remove\[Chi]s[sdx[sdx[expr,x,s1],x,s2],dxsqRule[#,s1,s2]&]];
dxsq[expr_?ConstQ,x_]:=0;


fdxsq[expr_,x_]:=fastApply[dxsq[#,x]&][expr];


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Eta]\[PartialD]x\[Theta]*)


(*\[PartialD]\[Eta]\[PartialD]x\[Theta]b*)
(*Define the fermion numbers of this operator*)
d\[Eta]dx\[Theta]Mod/:FN[d\[Eta]dx\[Theta]Mod[expr_,\[Eta]_,x_,\[Theta]b_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Eta]dx\[Theta]Rule[expr_,\[Chi]_,s_,\[Theta]b_]:=Module[{expanded,al=Unique[\[Alpha]],ad=Unique[\[Alpha]d],tempNCM},Plus@@((Flatten[tempNCM[up[\[Theta]b][ad],#/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.D\[Chi]low[\[Chi],al]/.\[Gamma]Dx[s,al,ad]/.tempNCM->NonCommutativeMultiply)&/@If[Head[expanded=Expand[expr]]===Plus,List@@expanded,{expanded}])];

(*Definition*)
d\[Eta]dx\[Theta]Mod[expr_Plus,\[Eta]_,x_,\[Theta]b_]:=Plus@@(d\[Eta]dx\[Theta]Mod[#,\[Eta],x,\[Theta]b]&/@List@@(expr));
d\[Eta]dx\[Theta]Mod[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Eta]_,x_,\[Theta]b_]:=Block[{\[Chi]1,s1},\[Chi]1=Unique["\[Chi]"];s1=Unique["s"];remove\[Chi]s[sdx[\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],x,s1],d\[Eta]dx\[Theta]Rule[#,\[Chi]1,s1,\[Theta]b]&]];
d\[Eta]dx\[Theta]Mod[expr_?ConstQ,\[Eta]_,x_,\[Theta]b_]:=0;


(* ::Subsection::Closed:: *)
(*\[PartialD]\[Theta]\[PartialD]\[Eta]*)


(*\[PartialD]\[Theta]\[PartialD]\[Eta]*)
(*Define the fermion numbers of this operator*)
d\[Theta]d\[Eta]Mod/:FN[d\[Theta]d\[Eta]Mod[expr_,\[Theta]_,\[Eta]_]]:=Mod[FN[expr]+1,2];

(*Define the differentiation rules*)
d\[Theta]d\[Eta]Rule[expr_,\[Chi]1_,\[Chi]2_]:=(expr/.D\[Chi]up[\[Chi]1,#1]/.D\[Chi]low[\[Chi]2,#1])&@Unique[\[Alpha]]; (*\[Chi]1\[Rule]\[Eta], \[Chi]2\[Rule]\[Theta]*)

(*Definition*)
d\[Theta]d\[Eta]Mod[expr_Plus,\[Theta]_,\[Eta]_]:=Plus@@(d\[Theta]d\[Eta]Mod[#,\[Theta],\[Eta]]&/@List@@(expr));
(***The auxiliary spinor for \[PartialD]\[Theta] must be recognized as bosonic.*)
d\[Theta]d\[Eta]Mod[expr_Times|expr_NonCommutativeMultiply|expr_d|expr_Power|expr_Symbol,\[Theta]_,\[Eta]_]:=Block[{\[Chi]1,\[Chi]2},\[Chi]1=Unique["\[Chi]"];\[Chi]2=Unique["\[Chi]"];remove\[Chi]s[\[Chi]d\[Theta][\[Chi]d\[Eta][expr,\[Eta],\[Chi]1],\[Theta],\[Chi]2],d\[Theta]d\[Eta]Rule[#,\[Chi]1,\[Chi]2]&]];
d\[Theta]d\[Eta]Mod[expr_?ConstQ,\[Theta]_,\[Eta]_]:=0;


(* ::Subsection::Closed:: *)
(*Chiral D and Db*)


(*This is just a wrapper of some already defined derivatives*)
(*If z_- = x + 2 \[ImaginaryI] \[Theta] \[Theta]b and z_+ = x - 2 \[ImaginaryI] \[Theta] \[Theta]b then D z_- = 0 and Db z_+ = 0*)
ChiralDm[ expr_,\[Eta]_,\[Theta]_,x_]:= -d\[Theta]d\[Eta][expr,\[Theta],\[Eta]] +  I d\[Eta]dx\[Theta][expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
ChiralDbm[expr_,\[Eta]_,\[Theta]_,x_]:=  d\[Theta]d\[Eta][expr,bar[\[Theta]],\[Eta]] -  I d\[Eta]dx\[Theta][expr/.sq[\[Theta]]->0,\[Eta],x,\[Theta]];
ChiralDp[ expr_,\[Eta]_,\[Theta]_,x_]:=  \[Eta]d\[Theta][expr,\[Eta],\[Theta]] +  I \[Eta]dx\[Theta][expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
ChiralDbp[expr_,\[Eta]_,\[Theta]_,x_]:=  \[Eta]d\[Theta][expr,\[Eta],bar[\[Theta]]] +  I \[Eta]dx\[Theta][expr/.sq[\[Theta]]->0,\[Eta],x,\[Theta]];

ChiralDsq[ expr_,\[Theta]_,x_]:= - 4 D[expr,sq[\[Theta]]] + 2 I d\[Theta]dx\[Theta][expr/.sq[bar[\[Theta]]]->0,\[Theta],x,bar[\[Theta]]] + sq[bar[\[Theta]]] (dxsq[expr/.sq[bar[\[Theta]]]->0,x]);
ChiralDbsq[expr_,\[Theta]_,x_]:=   4 D[expr,sq[bar[\[Theta]]]] - 2 I d\[Theta]dx\[Theta][expr/.sq[\[Theta]]->0,bar[\[Theta]],x,\[Theta]] -  sq[\[Theta]] (dxsq[expr/.sq[\[Theta]]->0,x]);


(*This is just a wrapper of some already defined derivatives*)
(*If z_- = x + 2 \[ImaginaryI] \[Theta] \[Theta]b and z_+ = x - 2 \[ImaginaryI] \[Theta] \[Theta]b then D z_- = 0 and Db z_+ = 0*)
ChiralQm[ expr_,\[Eta]_,\[Theta]_,x_]:= -d\[Theta]d\[Eta][expr,\[Theta],\[Eta]] -  I d\[Eta]dx\[Theta][expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
ChiralQbm[expr_,\[Eta]_,\[Theta]_,x_]:=  d\[Theta]d\[Eta][expr,bar[\[Theta]],\[Eta]] +  I d\[Eta]dx\[Theta][expr/.sq[\[Theta]]->0,\[Eta],x,\[Theta]];
ChiralQp[ expr_,\[Eta]_,\[Theta]_,x_]:=  \[Eta]d\[Theta][expr,\[Eta],\[Theta]] -  I \[Eta]dx\[Theta][expr/.sq[bar[\[Theta]]]->0,\[Eta],x,bar[\[Theta]]];
ChiralQbp[expr_,\[Eta]_,\[Theta]_,x_]:=  \[Eta]d\[Theta][expr,\[Eta],bar[\[Theta]]] -  I \[Eta]dx\[Theta][expr/.sq[\[Theta]]->0,\[Eta],x,\[Theta]];

ChiralQsq[ expr_,\[Theta]_,x_]:= - 4 D[expr,sq[\[Theta]]] - 2 I d\[Theta]dx\[Theta][expr/.sq[bar[\[Theta]]]->0,\[Theta],x,bar[\[Theta]]] + sq[bar[\[Theta]]] (dxsq[expr/.sq[bar[\[Theta]]]->0,x]);
ChiralQbsq[expr_,\[Theta]_,x_]:=   4 D[expr,sq[bar[\[Theta]]]] + 2 I d\[Theta]dx\[Theta][expr/.sq[\[Theta]]->0,bar[\[Theta]],x,\[Theta]] -  sq[\[Theta]] (dxsq[expr/.sq[\[Theta]]->0,x]);


(*These are aliases for when acting on the t*)
Chiral\[ScriptCapitalQ]m[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralDm[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalQ]bm[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralDbm[expr,\[Eta]b,\[Theta],x];
Chiral\[ScriptCapitalQ]p[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralDp[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalQ]bp[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralDbp[expr,\[Eta]b,\[Theta],x];


(*These are aliases for when acting on the t*)
Chiral\[ScriptCapitalD]m[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralQm[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalD]bm[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralQbm[expr,\[Eta]b,\[Theta],x];
Chiral\[ScriptCapitalD]p[  expr_,\[Eta]_,\[Theta]_,x_]:= ChiralQp[expr,\[Eta],\[Theta],x];
Chiral\[ScriptCapitalD]bp[expr_,\[Eta]b_,\[Theta]_,x_]:= ChiralQbp[expr,\[Eta]b,\[Theta],x];


(* ::Section::Closed:: *)
(*Replacements*)


(* ::Subsection::Closed:: *)
(*Replace \[Eta] with any expression with one \[Alpha] index*)


Replace\[Eta]ToAny::notaneta = "The symbol `1` given is not an \[Eta] variable";


Replace\[Eta]ToAny[expr_Plus,\[Eta]_,any_]:=Plus@@(Replace\[Eta]ToAny[#,\[Eta],any]&/@List@@(Expand[expr]));
Replace\[Eta]ToAny[expr_?ConstQ,\[Eta]_,any_]:=expr;
Replace\[Eta]ToAny[expr_Symbol,\[Eta]_,any_]:=expr;
Replace\[Eta]ToAny[expr_^y_,\[Eta]_,any_]:=Power[Replace\[Eta]ToAny[expr,\[Eta],any],y];
Replace\[Eta]ToAny[expr1_ expr2_^y_,\[Eta]_,any_]:=Replace\[Eta]ToAny[expr1,\[Eta],any] Power[Replace\[Eta]ToAny[expr2,\[Eta],any],y];

Replace\[Eta]ToAny[expr_Times|expr_NonCommutativeMultiply|expr_d,\[Eta]_,any_]/;FreeQ[expr,Power]:=Module[
{n\[Eta],\[Chi]s,i=1,exprw\[Chi],anyRule,Sumremove\[Chi]s,tempNCM,expanded,uplow},

n\[Eta] = Count[List@@expr/.{NonCommutativeMultiply->Sequence,Times->Sequence,d->Sequence},\[Eta]];
\[Chi]s=If[\[Eta]Q[\[Eta]],Table[Unique["\[Chi]1"],{n,1,n\[Eta]}],Message[Replace\[Eta]ToAny::notaneta,\[Eta]];Return[]];
exprw\[Chi] = expr/.\[Eta]:>(\[Chi]s[[i++]]);
anyRule[\[Xi]1_]:=
  Table[uplow={low[\[Xi]1][a_]->Sequence@@(case[[1]][a]),up[\[Xi]1][a_]->Sequence@@(case[[2]][a])};
        Function[exp,Flatten[tempNCM[exp/.{Times->tempNCM,NonCommutativeMultiply->tempNCM}],Infinity,tempNCM]/.$Ruplow/.tempNCM->NonCommutativeMultiply]/.$Ruplow->uplow
   ,{case,any}];
Sumremove\[Chi]s[exp_,{rules__}]:=Sum[remove\[Chi]s[exp,r],{r,{rules}}];

Fold[Sumremove\[Chi]s,exprw\[Chi],anyRule/@\[Chi]s]
];



(* ::Section::Closed:: *)
(*Comparing expressions*)


(* ::Subsection::Closed:: *)
(*Definition of the numerical values*)


(*If I insert the ordering rule for ** the numerical values comparison fails. Only for one coefficient though... Find out what goes wrong.*)


\[Gamma]0={{1,0},{0,1}};\[Gamma]1={{0,1},{1,0}};\[Gamma]2={{1,0},{0,-1}}; (*These are \[Gamma]_\[Mu]\[Alpha]\[Beta]*)
\[Epsilon]={{0,1},{-1,0}}; (*This is \[Epsilon]^\[Alpha]\[Beta]*)
g[m_,n_]:=If[m==n,Switch[m,1,-1,2,1,3,1],0];


(*The numerical contraction d \[Rule] nd*)
nd[x_n,y_n]:=x . \[Epsilon] . y/.n->List;

nd[a_n,x__,c_n]:=a . Dot[x] . c/.{n->List,b->List};

nd[v[x__],v[y__]]:=Sum[{x}[[m]]*{y}[[m]]*g[m,m],{m,1,3}];

nd[\[Epsilon]\[Mu]\[Nu]\[Rho],v[v1__,\[Mu]],v[v2__,\[Nu]],v[v3__,\[Rho]]]:=Sum[(-1)LeviCivitaTensor[3][[k,l,m]]{v1}[[k]]{v2}[[l]]{v3}[[m]],{k,1,3},{l,1,3},{m,1,3}];
nd[\[Epsilon]\[Mu]\[Nu]\[Rho],v[v1__,\[Mu]],v[v2__,\[Nu]],e_n,\[Gamma][\[Rho]],f_n]:=Sum[(-1)LeviCivitaTensor[3][[k,l,nn]]{v1}[[k]]{v2}[[l]](e . {\[Gamma]0,\[Gamma]1,\[Gamma]2}[[nn]] . f/.{n->List,b->List})g[nn,nn],{k,1,3},{l,1,3},{nn,1,3}];


nd[x_?NumericQ]:=x;
nd[x_?ConstQ]:=x;


(*Replacement rule for rational values*)
rule[\[Eta]s_,\[Theta]s_,xs_][H1_List,H2_List,H1i_List,H2i_List,Th1_List,Th2_List,Th1i_List,Th2i_List,xa_List,xb_List,xc_List]:=Module[{\[Eta]List,\[Theta]List,xList,xxList},
\[Eta]List = Table[\[Eta]s[[e]] -> n[H1[[e]]+I H1i[[e]],H2[[e]]+I H2i[[e]]],{e,1,Length[\[Eta]s]}];

\[Theta]List = Join[Table[\[Theta]s[[e]] -> n[Th1[[e]]+I Th1i[[e]],Th2[[e]]+I Th2i[[e]]],{e,1,Length[\[Theta]s]}],
    Table[bar[\[Theta]s[[e]]] -> n[Th1[[e]]-I Th1i[[e]],Th2[[e]]-I Th2i[[e]]],{e,1,Length[\[Theta]s]}]];

xList = Join[Table[xs[[e]] -> xa[[e]] \[Gamma]1 + xb[[e]] \[Gamma]2 + xc[[e]] \[Gamma]0,{e,1,Length[xs]}],
    Table[sq[xs[[e]]] -> -Det[xa[[e]] \[Gamma]1 + xb[[e]] \[Gamma]2 + xc[[e]] \[Gamma]0],{e,1,Length[xs]}],
    Table[glue[xs[[e]],"v"][m_] -> v[xc[[e]] ,xa[[e]], xb[[e]],m],{e,1,Length[xs]}]];

xxList = Flatten[Table[glue[xs[[e]],xs[[f]]] -> nd[v[xc[[e]] ,xa[[e]], xb[[e]]],v[xc[[f]] ,xa[[f]], xb[[f]]]],{e,1,Length[xs]},{f,1,Length[xs]}]];

Return[Join[\[Eta]List,\[Theta]List,xList,xxList]]
];


(*Replacement rule for random rational values*)
denom = 19;

ran[\[Eta]s_,\[Theta]s_,xs_]:=rule[\[Eta]s,\[Theta]s,xs][
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Eta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@\[Theta]s]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom,
    RandomInteger[{-denom,denom},Length@xs]/denom
]


(* ::Subsection::Closed:: *)
(*Compare an expression to zero*)


Options[Compare]={expReplacement -> {(*q->2,qb\[Rule]2,\[ScriptL]->2,l->2,k->1,\[CapitalDelta]\[Rule]4,j->0,\[CapitalDelta]\[Phi]->1*)},(*Replacements for the exponents of e.g. x12sq^\[CapitalDelta]*)
                  variables -> ConstantsList,(*Usual set of symbols treated as constants, which are the unknowns of this system*)
                  verbose -> 0,  (*Displays more info on the system that is being solved*)
                  minEqns -> -1,  (*Writes at least mineqns equations, if -1 the number of equations equals the number of variables matched*)
                  wrapfunction -> (#&)}; (*Wraps the equations of the randomly generated system with this function*)

(*Puts an overall minus to terms that contain \[Theta]s or \[Theta]bs in a non-canonical order, so that the replacement of numerical values is consistent*)
FixSigns[a_?ConstQ*x_]:=a FixSigns[x];
FixSigns[a_?ConstQ]:=a;
FixSigns[x_Plus]:=Plus@@(FixSigns/@(Apply[List,x]));
FixSigns[mon_Times|mon_NonCommutativeMultiply|mon_d]:=Module[{tempNCM},
    (Flatten[mon/.{Times->tempNCM,NonCommutativeMultiply->tempNCM,d->tempNCM},Infinity,tempNCM]/.{tempNCM[seq__] :> Signature[Cases[{seq},_?\[Theta]\[Theta]bQ]]})
    *mon
];


prepareCompare[expr_, OptionsPattern[Compare]]:=Module[{vars,powerRule,zeroExpr,all,\[Eta]Lis,\[Theta]Lis,xLis,\[Theta]sqs,vPrint,vvPrint,ruledelayed},
    (*Prints if verbose \[Rule] >0*)
    vPrint[str__]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,Print[str],Null];
    vvPrint[{str__},{str2__}]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,If[OptionValue[verbose]>1,Print[str2],Print[str]],Null];

    (*Sets a rule to treat the list usualVariables as constants for the theta Algebra*)
    ToExpression[
        StringJoin@@Table[
            toString[con]<>"/: ConstQ["<>toString[con]<>"] := True; ",
        {con,OptionValue[variables]}]];

    vars = Cases[Variables[expr],Alternatives@@OptionValue[variables]];
    vPrint["Solving for ",vars,"."];
    
    powerRule = {Power[b_,e_]:>Power[b,e/.OptionValue[expReplacement]]};
    all = Join[Cases[expr,x_d:>Sequence@@x,Infinity(*,-1*)],Cases[expr,x_?sqQ,{-1}(*,-1*)],Cases[expr,x_?xxQ,{-1}(*,-1*)]];
    all = DeleteDuplicates[all/.{x_?\[Theta]bQ :> unbar[x],
          x_?\[Gamma]Q->Nothing,x_Power->Nothing,
          x_?xvQ :> ToExpression[StringDrop[toString[Head[x]],-1]],
          x_?sqQ :> ToExpression[StringDrop[toString[x],-2]],
          x_?xxQ :> Sequence@@(ToExpression/@StringCases[toString[x],"x"~~(DigitCharacter..)])}];
    (*List of variables that will get assigned numerical values*)
    \[Eta]Lis = Select[all,\[Eta]Q];
    \[Theta]Lis = Select[all,\[Theta]Q];
    xLis = Select[all,xQ];
    vPrint["Replacing numerical values to ",\[Eta]Lis," ",\[Theta]Lis," ",xLis,"."];

    \[Theta]sqs=Join[sq/@#,sq[bar[#]]&/@#]&@\[Theta]Lis; If[\[Theta]sqs === {}, \[Theta]sqs = {\[Theta]0sq}];
    
    (*Now it collects similarly as fastApply before applying the FixSigns*)
    zeroExpr = Collect[
        Rex[
            Join[
                Table[ruledelayed[th \[ScriptD][yPattern___], \[ScriptD][yPattern,th]]/.ruledelayed->RuleDelayed,{th,\[Theta]sqs}],
                Table[ruledelayed[xsqr^pPattern_. \[ScriptD][yPattern___], \[ScriptD][yPattern,xsqr^pPattern]]/.ruledelayed->RuleDelayed,{xsqr,sq/@xLis}],
               {HoldPattern[NonCommutativeMultiply[x__]\[ScriptD][y___]]:>\[ScriptD][y,NonCommutativeMultiply[x]],d[x__]^j_. \[ScriptD][y___]:>\[ScriptD][y,d[x]^j],
                A_^pow_ \[ScriptD][y___] /; Not[ConstQ[A]] :> \[ScriptD][y,A^pow]}
            ]][
           (expr) \[ScriptD][]]
    ,\[ScriptD][___],coefficientwrapper];
    zeroExpr = zeroExpr /. {
               \[ScriptD][a___] :> \[ScriptD][FixSigns[Times[a]]/.powerRule/.d->nd]
               } //. Flatten[Table[{ruledelayed[\[ScriptD][yPattern_ th],th \[ScriptD][yPattern]],ruledelayed[\[ScriptD][th],th \[ScriptD][]]}/.ruledelayed->RuleDelayed,{th,\[Theta]sqs}]] /. {\[ScriptD][x_] :> x, \[ScriptD][] -> 1};
               
    Return[<|"zeroExpr" -> zeroExpr, "\[Eta]Lis" -> \[Eta]Lis, "\[Theta]Lis" -> \[Theta]Lis, "xLis" -> xLis, "\[Theta]sqs" -> \[Theta]sqs, "vars" -> vars|>]
];



(*Solves expr \[Congruent] \[ScriptCapitalA] t1 + \[ScriptCapitalB] t2 + ... = 0 for \[ScriptCapitalA],\[ScriptCapitalB],..*)
Compare[expr_, options: OptionsPattern[]]:=Module[{assoc, vars,zeroExpr,\[Eta]Lis,\[Theta]Lis,xLis,\[Theta]sqs,eqnlist={},vPrint,vvPrint, wrap},
    (*Prints if verbose \[Rule] >0*)
    vPrint[str__]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,Print[str],Null];
    vvPrint[{str__},{str2__}]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,If[OptionValue[verbose]>1,Print[str2],Print[str]],Null];
    
    wrap = OptionValue[wrapfunction];

    assoc = prepareCompare[expr, options];
    zeroExpr = assoc["zeroExpr"];
    \[Eta]Lis = assoc["\[Eta]Lis"];
    \[Theta]Lis = assoc["\[Theta]Lis"];
    xLis = assoc["xLis"];
    \[Theta]sqs = assoc["\[Theta]sqs"];
    vars = assoc["vars"];
    
    (*Solves the linear system for vars - ForceCoeffs*)

    Do[
    AppendTo[eqnlist,wrap/@(Flatten[Normal[CoefficientArrays[zeroExpr/.ran[\[Eta]Lis,\[Theta]Lis,xLis],\[Theta]sqs]]]/.coefficientwrapper[x_]:>x)];
    vvPrint[{Variables[Last[eqnlist]]},{Last[eqnlist]}];
    ,{i,1,Max[Length@vars,OptionValue[minEqns]]}];
    
    Solve[
        #==0&/@DeleteCases[Collect[#,vars,Collect[#,OptionValue[expReplacement][[All,1]]]&]&/@Flatten[eqnlist],0]
        ,vars
    ]/.Rule[symb_,something_]:> Rule[symb,Collect[something,vars,Factor]]
];


(*Solves expr \[Congruent] \[ScriptCapitalA] t1 + \[ScriptCapitalB] t2 + ... = 0 for \[ScriptCapitalA],\[ScriptCapitalB],..*)
fastCompare[expr_, options: OptionsPattern[Compare]]:=Module[{assoc, vars,zeroExpr,\[Eta]Lis,\[Theta]Lis,xLis,\[Theta]sqs,eqnlist={},vPrint,vvPrint, wrap, remember={}, appendRemember, sol},
    (*Prints if verbose \[Rule] >0*)
    vPrint[str__]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,Print[str],Null];
    vvPrint[{str__},{str2__}]:=If[(OptionValue[verbose]/.{True->1,False->0})>0,If[OptionValue[verbose]>1,Print[str2],Print[str]],Null];
    
    wrap = OptionValue[wrapfunction];
    
    appendRemember[x_] := (AppendTo[remember,glue[temp\[ScriptCapitalZ], Length[remember]+1] -> x];
                           ToExpression["temp\[ScriptCapitalZ]"<>ToString[Length[remember]]<>"/: ConstQ[temp\[ScriptCapitalZ]"<>ToString[Length[remember]]<>"]:=True"];
                           glue[temp\[ScriptCapitalZ], Length[remember]]);

    assoc = prepareCompare[expr, options];
    zeroExpr = assoc["zeroExpr"]/.coefficientwrapper[x_]:>appendRemember[x];
    \[Eta]Lis = assoc["\[Eta]Lis"];
    \[Theta]Lis = assoc["\[Theta]Lis"];
    xLis = assoc["xLis"];
    \[Theta]sqs = assoc["\[Theta]sqs"];
    vars = assoc["vars"];
    
    (*Solves the linear system for vars - ForceCoeffs*)

    Do[
    AppendTo[eqnlist,wrap/@(Flatten[Normal[CoefficientArrays[zeroExpr/.ran[\[Eta]Lis,\[Theta]Lis,xLis],\[Theta]sqs]]]/.coefficientwrapper[x_]:>x)];
    vvPrint[{Variables[Last[eqnlist]]},{Last[eqnlist]}];
    ,{i,1,Max[Length@vars,OptionValue[minEqns]]}];
    
    sol = Quiet[Solve[
        #==0&/@DeleteCases[Collect[#,vars,Collect[#,OptionValue[expReplacement][[All,1]]]&]&/@Flatten[eqnlist],0]
        ,remember[[All,1]]
    ],Solve::svars];
    vPrint["The intermediate solution is: ",sol];
    
    If[Length[sol] == 0, sol,
    Solve[sol[[1]]/.remember/.Rule->Equal,vars]/.Rule[symb_,something_]:> Rule[symb,Collect[something,vars,Factor]]
    ]
];


(* ::Section::Closed:: *)
(*Expansion of tensor structure elements*)


(* ::Subsection::Closed:: *)
(*General*)


orderexp = 4;
tokeep = {Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]b1,Global`\[Theta]b2,
          Global`\[Theta]1sq,Global`\[Theta]b1sq,Global`\[Theta]2sq,Global`\[Theta]b2sq};

tokeepSet[tk_]:=Module[{},
    \[Theta]tozero=Flatten[{low[#][_]->0,up[#][_]->0}&/@Complement[{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3},tk]];
    \[Theta]sqtozero=#->0&/@Complement[{Global`\[Theta]1sq,Global`\[Theta]2sq,Global`\[Theta]3sq,Global`\[Theta]b1sq,Global`\[Theta]b2sq,Global`\[Theta]b3sq},tk];
]


tokeepSet[tokeep];


beforeContract[expr_]:=Expand[expr]/.\[Theta]sqtozero/.{(Global`\[Theta]1sq|Global`\[Theta]b1sq|Global`\[Theta]2sq|Global`\[Theta]b2sq|Global`\[Theta]3sq|Global`\[Theta]b3sq)^p_ :> 0};


(* ::Subsection::Closed:: *)
(*Functions of two points*)


boldxij[i_,j_][al_,be_]:=Module[{m=Unique["\[Mu]"],xx},
    xx = If[i<j,v[glue["x",i,j]][m],-v[glue["x",j,i]][m]];
    xx \[Gamma][m][al,be] + 
    2I (low[glue["\[Theta]",i]][al]**low[glue["\[Theta]b",j]][be] + low[glue["\[Theta]b",i]][al]**low[glue["\[Theta]",j]][be]) - 
    I  (low[glue["\[Theta]",i]][al]**low[glue["\[Theta]b",i]][be] + low[glue["\[Theta]b",i]][al]**low[glue["\[Theta]",i]][be]) -
    I  (low[glue["\[Theta]",j]][al]**low[glue["\[Theta]b",j]][be] + low[glue["\[Theta]b",j]][al]**low[glue["\[Theta]",j]][be])/.\[Theta]tozero
];


yij[i_,j_][al_,be_]:=Module[{m=Unique["\[Mu]"],xx},
    xx = If[i<j,v[glue["x",i,j]][m],-v[glue["x",j,i]][m]];
    xx \[Gamma][m][al,be] + 
    2I (low[glue["\[Theta]",i]][al]**low[glue["\[Theta]b",j]][be] + low[glue["\[Theta]",i]][be]**low[glue["\[Theta]b",j]][al]) - 
    I  (low[glue["\[Theta]",i]][al]**low[glue["\[Theta]b",i]][be] + low[glue["\[Theta]",i]][be]**low[glue["\[Theta]b",i]][al]) -
    I  (low[glue["\[Theta]",j]][al]**low[glue["\[Theta]b",j]][be] + low[glue["\[Theta]",j]][be]**low[glue["\[Theta]b",j]][al])/.\[Theta]tozero
];


ybij[i_,j_][al_,be_]:=Module[{m=Unique["\[Mu]"],xx},
    xx = If[i<j,v[glue["x",i,j]][m],-v[glue["x",j,i]][m]];
    xx \[Gamma][m][al,be] + 
    2I (low[glue["\[Theta]b",i]][al]**low[glue["\[Theta]",j]][be] + low[glue["\[Theta]b",i]][be]**low[glue["\[Theta]",j]][al]) + 
    I  (low[glue["\[Theta]",i]][al]**low[glue["\[Theta]b",i]][be] + low[glue["\[Theta]",i]][be]**low[glue["\[Theta]b",i]][al]) +
    I  (low[glue["\[Theta]",j]][al]**low[glue["\[Theta]b",j]][be] + low[glue["\[Theta]",j]][be]**low[glue["\[Theta]b",j]][al])/.\[Theta]tozero
];


\[Theta]ij[i_,j_][\[Alpha]_]:=up[glue["\[Theta]",i]][\[Alpha]]-up[glue["\[Theta]",j]][\[Alpha]]/.\[Theta]tozero;
\[Theta]bij[i_,j_][\[Alpha]d_]:=up[glue["\[Theta]b",i]][\[Alpha]d]-up[glue["\[Theta]b",j]][\[Alpha]d]/.\[Theta]tozero;


boldxijsq[i_,j_]:=boldxijsq[i,j]=Module[{\[Alpha]=Unique["\[Alpha]"],\[Beta]=Unique["\[Beta]"],\[Alpha]2=Unique["\[Gamma]"],\[Beta]2=Unique["\[Delta]"]},
    -1/2 Contraction[boldxij[i,j][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha],\[Alpha]2]\[CurlyEpsilon][\[Beta],\[Beta]2]boldxij[i,j][\[Alpha]2,\[Beta]2]] /.\[Theta]sqtozero
];
yijsq[i_,j_]:=yijsq[i,j]=Module[{\[Alpha]=Unique["\[Alpha]"],\[Beta]=Unique["\[Beta]"],\[Alpha]2=Unique["\[Gamma]"],\[Beta]2=Unique["\[Delta]"]},
    -1/2 Contraction[yij[i,j][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha],\[Alpha]2]\[CurlyEpsilon][\[Beta],\[Beta]2]yij[i,j][\[Alpha]2,\[Beta]2]] /.\[Theta]sqtozero
];
ybijsq[i_,j_]:=ybijsq[i,j]=Module[{\[Alpha]=Unique["\[Alpha]"],\[Beta]=Unique["\[Beta]"],\[Alpha]2=Unique["\[Gamma]"],\[Beta]2=Unique["\[Delta]"]},
    -1/2 Contraction[ybij[i,j][\[Alpha],\[Beta]]\[CurlyEpsilon][\[Alpha],\[Alpha]2]\[CurlyEpsilon][\[Beta],\[Beta]2]ybij[i,j][\[Alpha]2,\[Beta]2]] /.\[Theta]sqtozero
];


\[Eta]boldx\[Eta]ToThejSaved[i_,j_][arg\[Eta]1_,arg\[Eta]2_] := \[Eta]boldx\[Eta]ToThejSaved[i,j][arg\[Eta]1,arg\[Eta]2] = Module[{xx,\[Alpha]=Unique["\[Alpha]"],\[Beta]=Unique["\[Beta]"],contracted},
    xx = If[i<j,glue["x",i,j],-glue["x",j,i]];
    contracted = Contraction[boldxij[i,j][\[Alpha],\[Beta]] up[arg\[Eta]1][\[Alpha]] up[arg\[Eta]2][\[Beta]]] /.\[Theta]sqtozero;
    FPower[contracted,d[arg\[Eta]1,xx,arg\[Eta]2],$J,orderexp] /.\[Theta]sqtozero
];
\[Eta]boldx\[Eta]ToThej[i_,j_][arg\[Eta]1_,arg\[Eta]2_][JJ_] := \[Eta]boldx\[Eta]ToThejSaved[i,j][arg\[Eta]1,arg\[Eta]2] /. $J -> JJ


boldxijsqToTheQSaved[i_,j_]:=boldxijsqToTheQSaved[i,j]=fastReduction[FPower[boldxijsq[i,j],If[i<j,glue["x",i,j,"sq"],glue["x",j,i,"sq"]],$Q,orderexp]/.\[Theta]sqtozero];
boldxijsqToTheQ[i_,j_][QQ_]:=boldxijsqToTheQSaved[i,j]/.{$Q->QQ};
yijsqToTheQSaved[i_,j_]:=yijsqToTheQSaved[i,j]=fastReduction[FPower[yijsq[i,j],If[i<j,glue["x",i,j,"sq"],glue["x",j,i,"sq"]],$Q,orderexp]/.\[Theta]sqtozero];
yijsqToTheQ[i_,j_][QQ_]:=yijsqToTheQSaved[i,j]/.{$Q->QQ};
ybijsqToTheQSaved[i_,j_]:=ybijsqToTheQSaved[i,j]=fastReduction[FPower[ybijsq[i,j],If[i<j,glue["x",i,j,"sq"],glue["x",j,i,"sq"]],$Q,orderexp]/.\[Theta]sqtozero];
ybijsqToTheQ[i_,j_][QQ_]:=ybijsqToTheQSaved[i,j]/.{$Q->QQ};


boldxijInv[i_,j_][al_,be_]:=Module[{aa=Unique["\[Alpha]"],bb=Unique["\[Beta]"]},
    -boldxijsqToTheQ[i,j][-1] \[CurlyEpsilon][be,aa]\[CurlyEpsilon][al,bb] boldxij[i,j][aa,bb]
];


vij[i_,j_]:=vij[i,j]=Module[{aa=Unique["\[Alpha]"],bb=Unique["\[Beta]"]},
    1 + 2 I Contraction[\[Theta]ij[i,j][aa]**\[Theta]bij[i,j][bb] boldxij[i,j][aa,bb]] boldxijsqToTheQ[i,j][-1] /.\[Theta]sqtozero
];
vMinusOneij[i_,j_]:=vij[i,j]=Module[{aa=Unique["\[Alpha]"],bb=Unique["\[Beta]"]},
    1 + 2 I Contraction[\[Theta]bij[i,j][aa]**\[Theta]ij[i,j][bb] boldxij[i,j][aa,bb]] boldxijsqToTheQ[i,j][-1] /.\[Theta]sqtozero
];


vijToTheQSaved[i_,j_]:=vijToTheQSaved[i,j]=fastReduction[FPower[vij[i,j],1,$Q,orderexp]/.\[Theta]sqtozero];
vijToTheQ[i_,j_][QQ_]:=vijToTheQSaved[i,j]/.{$Q->QQ};


(* ::Subsection::Closed:: *)
(*Functions of three points*)


\[Eta]Replacement[i_]:=general\[Eta]Replacement[i]=Module[{ad=Unique["\[Alpha]"],be=Unique["\[Beta]"],expand},
    Map[Flatten[#,\[Infinity],tempNCM]/.tempNCM[X__]:>Function[{X}]&,
        Thread[((If[Head[expand=Expand[#]]===Plus,List@@expand,{expand}]/.{
            Times->tempNCM,NonCommutativeMultiply->tempNCM})&/@{
            up[glue["\[Eta]",i]][ad] boldxij[i,3][ad,#],
            \[CurlyEpsilon][#,be]up[glue["\[Eta]",i]][ad] boldxij[i,3][ad,be]})]
    ,{2}]];


boldX3[al_,be_]:=Module[{aa=Unique["\[Alpha]"],bb=Unique["\[Alpha]"],cc=Unique["\[Alpha]"],dd=Unique["\[Alpha]"],ee=Unique["\[Alpha]"],ff=Unique["\[Alpha]"]},
    - \[CurlyEpsilon][al,aa]\[CurlyEpsilon][bb,cc] boldxij[1,3][cc,aa]
    boldxij[1,2][bb,dd]
    \[CurlyEpsilon][dd,ee]\[CurlyEpsilon][be,ff] boldxij[3,2][ff,ee]
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-1,orderexp]
];
boldX3NoPref[al_,be_]:=Module[{aa=Unique["\[Alpha]"],bb=Unique["\[Alpha]"],cc=Unique["\[Alpha]"],dd=Unique["\[Alpha]"],ee=Unique["\[Alpha]"],ff=Unique["\[Alpha]"]},
    - \[CurlyEpsilon][al,aa]\[CurlyEpsilon][bb,cc] boldxij[1,3][cc,aa]
    boldxij[1,2][bb,dd]
    \[CurlyEpsilon][dd,ee]\[CurlyEpsilon][be,ff] boldxij[3,2][ff,ee]
];
X3[al_,be_]:=Module[{aa=Unique["\[Alpha]"],bb=Unique["\[Alpha]"]},
    boldX3[al,be] - I \[CurlyEpsilon][al,be] \[CurlyEpsilon][bb,aa] Theta3[aa]**Thetab3[bb]
];
X3NoPref[al_,be_]:=Module[{aa=Unique["\[Alpha]"],bb=Unique["\[Alpha]"],cc=Unique["\[Alpha]"],dd=Unique["\[Alpha]"],ee=Unique["\[Alpha]"],ff=Unique["\[Alpha]"]},
    boldxijsq[1,3]boldxijsq[2,3] boldX3NoPref[al,be] - I \[CurlyEpsilon][al,be] \[CurlyEpsilon][bb,aa] Theta3NoPref[aa]**Thetab3NoPref[bb]
];


Theta3[al_]:=Module[{ga=Unique["\[Alpha]"],de=Unique["\[Beta]"],ep=Unique["\[Gamma]"]},
    \[CurlyEpsilon][al,ga] \[CurlyEpsilon][de,ep] boldxijInv[1,3][ga,ep] \[Theta]ij[3,1][de] - \[CurlyEpsilon][al,ga] \[CurlyEpsilon][de,ep] boldxijInv[2,3][ga,ep] \[Theta]ij[3,2][de] //beforeContract
];
Thetab3[al_]:=Module[{ga=Unique["\[Alpha]"],de=Unique["\[Beta]"],ep=Unique["\[Gamma]"]},
    \[CurlyEpsilon][al,ga] \[CurlyEpsilon][de,ep] boldxijInv[1,3][ga,ep] \[Theta]bij[3,1][de] - \[CurlyEpsilon][al,ga] \[CurlyEpsilon][de,ep] boldxijInv[2,3][ga,ep] \[Theta]bij[3,2][de] //beforeContract
];
Theta3NoPref[al_]:=Module[{ga=Unique["\[Alpha]"],de=Unique["\[Beta]"],ep=Unique["\[Gamma]"]},
    -boldxijsq[2,3] boldxij[1,3][de,al] \[Theta]ij[3,1][de] + boldxijsq[1,3] boldxij[2,3][de,al] \[Theta]ij[3,2][de] //beforeContract
];
Thetab3NoPref[al_]:=Module[{ga=Unique["\[Alpha]"],de=Unique["\[Beta]"],ep=Unique["\[Gamma]"]},
    -boldxijsq[2,3] boldxij[1,3][de,al] \[Theta]bij[3,1][de] + boldxijsq[1,3] boldxij[2,3][de,al] \[Theta]bij[3,2][de] //beforeContract
];


\[Eta]X\[Eta]Saved[]:=\[Eta]X\[Eta]Saved[]=fastReduction[Module[{al=Unique["\[Alpha]"],be=Unique["\[Beta]"]},
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-2,orderexp]
    Contraction[low[Global`\[Eta]999][al] X3NoPref[al,be] low[Global`\[Eta]899][be] //beforeContract] /. \[Theta]sqtozero
]];
\[Eta]X\[Eta][et1_,et2_]:=\[Eta]X\[Eta]Saved[]/.{Global`\[Eta]999 -> et1, Global`\[Eta]899 -> et2};
(**)
\[Eta]X\[CapitalTheta]Saved[]:=\[Eta]X\[CapitalTheta]Saved[]=fastReduction[Module[{al=Unique["\[Alpha]"],be=Unique["\[Beta]"]},
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-3,orderexp]
    Contraction[low[Global`\[Eta]999][al] X3NoPref[al,be] Theta3NoPref[be] //beforeContract] /. \[Theta]sqtozero
]];
\[Eta]X\[CapitalTheta][et_] := \[Eta]X\[CapitalTheta]Saved[] /. Global`\[Eta]999 -> et;
(**)
\[Eta]X\[CapitalTheta]bSaved[]:=\[Eta]X\[CapitalTheta]bSaved[]=fastReduction[Module[{al=Unique["\[Alpha]"],be=Unique["\[Beta]"]},
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-3,orderexp]
    Contraction[low[Global`\[Eta]999][al] X3NoPref[al,be] Thetab3NoPref[be] //beforeContract] /. \[Theta]sqtozero
]];
\[Eta]X\[CapitalTheta]b[et_] := \[Eta]X\[CapitalTheta]bSaved[] /. Global`\[Eta]999 -> et;
(**)
\[CapitalTheta]X\[CapitalTheta]b[]:=\[CapitalTheta]X\[CapitalTheta]b[]=fastReduction[Module[{al=Unique["\[Alpha]"],be=Unique["\[Beta]"]},
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-4,orderexp]
    listwise[Contraction,","][Theta3NoPref[al]**Thetab3NoPref[be] Expand[Contraction[X3NoPref[al,be]]] //beforeContract] /. \[Theta]sqtozero
]];
(**)
Xsq[]:=Xsq[]=fastReduction[Module[{\[Alpha]=Unique["\[Alpha]"],\[Beta]=Unique["\[Beta]"],\[Alpha]2=Unique["\[Gamma]"],\[Beta]2=Unique["\[Delta]"]},
    -1/2 FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-4,orderexp]
    listwise[Contraction,","][Expand[Contraction[X3NoPref[\[Alpha],\[Beta]]]]\[CurlyEpsilon][\[Alpha],\[Alpha]2]\[CurlyEpsilon][\[Beta],\[Beta]2] Expand[Contraction[X3NoPref[\[Alpha]2,\[Beta]2]]] //beforeContract] /.\[Theta]sqtozero
]];


\[CapitalTheta]\[Eta]Saved[]:=\[CapitalTheta]\[Eta]Saved[]=fastReduction[Module[{al=Unique["\[Alpha]"]},
   Contraction[-Theta3[al] up[Global`\[Eta]999][al]] 
]];
\[CapitalTheta]\[Eta][et_] := \[CapitalTheta]\[Eta]Saved[] /. Global`\[Eta]999 -> et;
(**)
\[CapitalTheta]b\[Eta]Saved[]:=\[CapitalTheta]b\[Eta]Saved[]=fastReduction[Module[{al=Unique["\[Alpha]"]},
   Contraction[-Thetab3[al] up[Global`\[Eta]999][al]] 
]];
\[CapitalTheta]b\[Eta][et_] := \[CapitalTheta]b\[Eta]Saved[] /. Global`\[Eta]999 -> et;
(**)
\[CapitalTheta]\[CapitalTheta]b[]:=\[CapitalTheta]\[CapitalTheta]b[]=fastReduction[Module[{al=Unique["\[Alpha]"],be=Unique["\[Beta]"]},
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-2,orderexp]
    Contraction[Theta3NoPref[be]**Thetab3NoPref[al] \[CurlyEpsilon][al,be] //beforeContract] 
]];
\[CapitalTheta]sq[]:=\[CapitalTheta]sq[]=fastReduction[Module[{al=Unique["\[Alpha]"],be=Unique["\[Beta]"]},
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-2,orderexp]
    Contraction[Theta3NoPref[be]**Theta3NoPref[al] \[CurlyEpsilon][al,be] //beforeContract] 
]];
\[CapitalTheta]bsq[]:=\[CapitalTheta]bsq[]=fastReduction[Module[{al=Unique["\[Alpha]"],be=Unique["\[Beta]"]},
    FPower[boldxijsq[1,3]boldxijsq[2,3],Global`x13sq Global`x23sq,-2,orderexp]
    Contraction[Thetab3NoPref[be]**Thetab3NoPref[al] \[CurlyEpsilon][al,be] //beforeContract] 
]];


XsqToTheQSaved[]:=XsqToTheQSaved[]=fastReduction[FPower[Xsq[],Global`x12sq/(Global`x13sq Global`x23sq),$Q,orderexp]/.\[Theta]sqtozero];
XsqToTheQ[QQ_]:=XsqToTheQSaved[]/.{$Q->QQ};


\[Eta]X\[Eta]ToTheJSaved[]:=\[Eta]X\[Eta]ToTheJSaved[]=fastReduction[FPower[\[Eta]X\[Eta]Saved[],SetToZero[\[Eta]X\[Eta]Saved[],{Global`\[Theta]1,Global`\[Theta]2,Global`\[Theta]3,Global`\[Theta]b1,Global`\[Theta]b2,Global`\[Theta]b3}],$J,orderexp]/.\[Theta]sqtozero];
\[Eta]X\[Eta]ToTheJ[et1_,et2_][JJ_]:=\[Eta]X\[Eta]ToTheJSaved[]/.{$J->JJ, Global`\[Eta]999 -> et1, Global`\[Eta]899 -> et2};


kinPrefactor[{\[CapitalDelta]1_,j1_,R1_},{\[CapitalDelta]2_,j2_,R2_}]/;(R1 =!= 0 || R2 =!= 0):=kinPrefactor[{\[CapitalDelta]1,j1,R1},{\[CapitalDelta]2,j2,R2}]=fastReduction[
    yijsqToTheQ[1,3][(-\[CapitalDelta]1-R1-j1/2)/2] ybijsqToTheQ[1,3][(-\[CapitalDelta]1+R1-j1/2)/2]
    yijsqToTheQ[2,3][(-\[CapitalDelta]2-R2-j2/2)/2] ybijsqToTheQ[2,3][(-\[CapitalDelta]2+R2-j2/2)/2]
];
kinPrefactor[{\[CapitalDelta]1_,j1_,0},{\[CapitalDelta]2_,j2_,0}]:=kinPrefactor[{\[CapitalDelta]1,j1,0},{\[CapitalDelta]2,j2,0}]=fastReduction[
    boldxijsqToTheQ[1,3][(-\[CapitalDelta]1-j1/2)]
    boldxijsqToTheQ[2,3][(-\[CapitalDelta]2-j2/2)]
];


(* ::Section::Closed:: *)
(*Two and three-point functions*)


(* ::Subsection::Closed:: *)
(*Supersymmetric two-point function*)


NonSUSY2pf[\[CapitalDelta]_,j_]:=Module[{m\[Eta]1,m\[Eta]2,m\[Eta]b1,m\[Eta]b2,mx12},
    m\[Eta]1  = Global`\[Eta]1;
    m\[Eta]2  = Global`\[Eta]2;
    mx12 = Global`x12;
    
    (-I)^(j) (d[m\[Eta]1,mx12,m\[Eta]2]^j)/sq[mx12]^(\[CapitalDelta]+j/2)
];


twopfjjbqqbText = "1:eJztXc9zGzUUTmoncTuF4cRw4NL/oOsyk8INOMNkAsce6kydzjJOnMZJKP8vB44cGCYzlMB63Tgr7ZP13ltJ+ySLgydbdrXS2/d974eepGcn8+PTw52dncWw+jmaXS9OP1le7VU/P5dn08Xpo+XlqPo5nlyV8/PJrNytLsrPqp/V/1veejT/dXq5ujyofr6fn13Mpu/LneWd/1X/Lb6s/vrpojyfX347ezs9uZy8ePP66LK8mVxNX/9yM5mtnr3vQ/kv45k7xjOoviEbXcuh/ut9MV68U9/2cJtJssvulPU/419Zt7UayLMND7365vmLr79ajuZxddfy6ruXxeJd46rq7+LJ+uqkUC/Xo9E+91IZ3tQfvb7xsGj8PS6HgJYM4McGH4XWeFz7WB9QH/gpSsgjr+rrW60GrbfJ0K3TvQ36Xv6Dl+TDQ4/aH8rahvbQqqG/FCnZGzo2NfQ3XhwD9ckhRvoCUQx8Az6KcTStSs45UiPhar7Q7Z0a2QZYm3mIwL1IXyylDZWOsd5mfegYeOiOSDP3XST3zgpIqHf1JZWp2tyUFTwJBWepUDAFZ8GPpeB2a+tSwR+jFNxLoGZX8Ds5Cv5ps2Nk1YVeftCWKc8pvSU6pXVDA6ChAadHNfdpPuoXtI9B8aO1N/3BGTs1HDB68R/YXvxTjIR4Tf/OFQnvdUAQjNM+qvf6IHL9daiGQPX5k8NWhNdpnQdoFNkaBFd+a7W1dhb8uTRHB22C3m7HSgkoeV4L4UXaV6w0Y9T49PA94+Y9hU/l2Ae+SNaLfvSi8f2btzRVAdCdcHoRKEjb5kkArU9DpR1zQANE7DLTh8YBjSPNh0I40ZXfhB6oux3Qkym5KyWvcbeHVNM17jbqJd4PcKC2OKI1DtDkmjIG6MqJcTDAnGT0k8GDIkdW1M9Gqhp8kiykS3fJ2A27XQvvtTm2OwA+O2Z6twQ9RKm4RQqC5KGQJDB8NAMrMxRaq5o/UO2aODODSnuR51lTNKhMrqEAUI0FgMpazJhcNZigibd9RYcBn87pRJP/yq9bCZVfeqeA2IIwzdFhKgENU3cxGaF2TG9+DDaPSatoRjFHbyL9T4+2rW1Z79tSrBhUjoFN0fWUIBBkK2R7Y4olA20D67vgzBnLlnisxkIkZ1gGKGx5IAXaUJYPA1QgusrmoZdIKqAOjSEdKrjmof6A2TzI9kAkmQdUXBWjeQhbzUSBtlr5lIEq1xbIphEh3g5f9TVFzPrve7Wjs4nOvrWuIGjdGCRcrC8FiFhcrjfF6RZFTujKl2gKtCiqbhx8rOV2FOxCsXQLeo7tSPp4gvrUP6f33Cu31iN4LSVFMaV6QFQ/r5tH32F5ttOy6bDVWBREdqj07guuTzrA1XHiLPnkbb3xkt7YgNMD6vpKqSkDT/MTnLBh9czn1c+P8/NKec+urypNuZn+cD27Ki9mv1mdw5cFfIsGuFFbVhlD8WpwYCWlZI8CKbSgqZcUtX/U7FjAiQ9WdlabLUHFt2RwUrd1MKbxqHukEKTB36XgFsNXve6d0o91hdcnHWqc12UlXyYj2lQsz2ulcoifgpw+1jxpAxthGFVu5CzXxw4Uh8ulwRzVe6bB2KN6nDPnIqqP1e9oB1JtxDVBqUX1IWdYUsCQNA2WENUb6kcCKbTcIoC8E73PkgABs+KE4wn8Z7ew1QQCSim8yo3KH6hCBMdEkTSAoT5J96KoYItxjFRg5HT5NqTLcbNOrKxyTpd7yqIlMBlt3ZRUISN4bUVPZJS9fBbfKecLxFT4K8jLj6pmOJDccHnmrG8cJzDrG89ejbWJHbz10caEWiqetKGIMdKiknrcY8QRcIxjpJJl5GNEERuzbiVnBRh1Kx2zAuJqWaA9ygIH+LmWxRv7JVfLQqVGuTmKraXTCCryXUx89rCu17CToi+PueCF84XkDSpZsgI8Tovn3csu7ZlM+iUT1e2BdmOOyvpaGKi9g8jD0OPzPFSIWxZSNemL6YD4KKbNDkg0xYN2B8Rjbaw8B8Tg6he8eRjRO2R3ZifcUicMLfnZMizvx+QpBkmbAjAxCG1qbLspAJca8bPBfqYA317AxineGNS5AOIHjMPNqLmOXlYq9C09JHsBmxAgnRkiBH4SwYnJM4keahhagrtILtqJXlZcWtKzo8HXQ6boT6hz2AOgT8Hyb/hdiPyFDRvTkxLksWnXTv+FQfLk4VY/qNPoSWXuYbMFZ+4lqoJbaFALCpKayTAcKAAnQXo5BDe7+P26+Ebmi0TbnTFfhOVi/pCeqxOSR3p86l7w1L0F9Og4DnFaCzLSDg3pFCPthFFFysMnhSpaWj17yiLsZ6/VffEFhu6RHgnFOUY6Bs3a4HbxOMpz575L+KOfeLED2RTybp+sMOdrwkFynjsXB/yY3IVtnDvn0RJjw4voZUU79rdJS5gd4lxX9TnjEqlH5bFSd/qgWauq8fwS077ZFBebEIgwTbKPtTYZEqHNa0y59nj3JeiMXLir8e5TQMm2E6hMPRLAb2Ihc5Us8y1+/1DyLLr/iDqb72y+s/l2iFyC+Y55m6HOVIZZ9Q8dGBdRvk9VfhlkFjaHM7RoESLfZbiFegAAJlu035ZqVj5/iyk08ksyH4laUtHUU7NQUk1o04UClMQkIBMMoyl0tcmb2l6pQEyXlr1DLbewi8rbFr+BRIXb28YqKs6BZNkr8Fd2ErdSGvHLwmaCVGcfJP08HTsd/g9saqwX"


SUSY2pf[\[CapitalDelta]_,j_,R_]:= Uncompress[twopfjjbqqbText] /. {SpinorAlgebra3d`Private`\[CapitalDelta]val -> \[CapitalDelta], SpinorAlgebra3d`Private`Rval ->  R, SpinorAlgebra3d`Private`jval -> j};


(* ::Section::Closed:: *)
(*End of package*)


(*End[];*)

(*EndPackage[];*)
